<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'edu');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'aN%ycFSl~k`R( s<+KHGJBO]28W 4V5kCT*={i$>Ku0L/I#MpFUguj5u8y`<[Iag');
define('SECURE_AUTH_KEY',  'wkHBKFV2-riH%q 6t,(ekmN<=S7Y!Oph!9SaHxH<~P>ym$Z73e,|7TDIVCA`D(|#');
define('LOGGED_IN_KEY',    'Df$xlD#-Tf/M8k>2xRwmLe +rbvq8S06Bz@w,LpN*_kos}$-x~,iG8s#w6V]ZVDu');
define('NONCE_KEY',        '`pBA,@,f{ng]w4cb~gR`2r?hw$HqGaYY(1u[`X^ebW_=e*KOCD9kE;Mn9`n$Q~Y@');
define('AUTH_SALT',        'zGpIw`x#I[+sfH`4AUaRgX/Ru{Eq-K@E/nG9W~$@bX:0Mrx%nd=S3qBVlHt`c*|<');
define('SECURE_AUTH_SALT', 'd&6NM|w5JT1-vXv:.MBnm&MYICxqEVh@A@$|o.XQI1k@Ku&}3(|SR<[}O/LIC0`:');
define('LOGGED_IN_SALT',   'Ne:vhdS*3r<`T~q[$6>R<dG,hx1N~=xD(gp4ER)6%##wb1~l)/,6mcG,63yCB_d:');
define('NONCE_SALT',       ']m!(0*u`t;K=FO)nh.@ew!;[Ot*s%:: |$;9J$eeM2EhJ>5uPPW:eOp8@|b4l@v2');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
