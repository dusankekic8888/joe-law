/**
 * @file
 * Homepage Collapsable Success Story.
 */

/* global jQuery, Drupal */

(function ($, Drupal) {
  'use strict';
  Drupal.behaviors.victoryAccessibilityBar = {
    attach: function (context, settings) {
      // Do not attach to AJAX responses.
      if (context !== window.document) {
        return;
      }
      // Keep all grouped screen reader elements visible if at least one of them
      // has focus.
      $('[data-sr-group-type="container"]', context).find('[data-sr-group-type="item"]').on('focus', function () {
        $(this).parents('[data-sr-group-type="container"]').find('[data-sr-group-type="item"]:not(.sr-only-focused)').addClass('sr-only-focused');
        // Notify everyone that screen-reader item received a focus.
        $(document).trigger('focus.sr-group-item', $(this));
      });

      // Remove all focused classes when close button is clicked. This allows
      // to preserve the bar element in the DOM and access it multiple times.
      $('[data-dismiss-sr]').click(function () {
        var $target = $($(this).attr('data-dismiss-sr'));
        $target.find('.sr-only-focused').removeClass('sr-only-focused');
      });
    }
  };
}(jQuery, Drupal));
;
/* ========================================================================
 * Bootstrap: transition.js v3.3.7
 * http://getbootstrap.com/javascript/#transitions
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // CSS TRANSITION SUPPORT (Shoutout: http://www.modernizr.com/)
  // ============================================================

  function transitionEnd() {
    var el = document.createElement('bootstrap')

    var transEndEventNames = {
      WebkitTransition : 'webkitTransitionEnd',
      MozTransition    : 'transitionend',
      OTransition      : 'oTransitionEnd otransitionend',
      transition       : 'transitionend'
    }

    for (var name in transEndEventNames) {
      if (el.style[name] !== undefined) {
        return { end: transEndEventNames[name] }
      }
    }

    return false // explicit for ie8 (  ._.)
  }

  // http://blog.alexmaccaw.com/css-transitions
  $.fn.emulateTransitionEnd = function (duration) {
    var called = false
    var $el = this
    $(this).one('bsTransitionEnd', function () { called = true })
    var callback = function () { if (!called) $($el).trigger($.support.transition.end) }
    setTimeout(callback, duration)
    return this
  }

  $(function () {
    $.support.transition = transitionEnd()

    if (!$.support.transition) return

    $.event.special.bsTransitionEnd = {
      bindType: $.support.transition.end,
      delegateType: $.support.transition.end,
      handle: function (e) {
        if ($(e.target).is(this)) return e.handleObj.handler.apply(this, arguments)
      }
    }
  })

}(jQuery);
;
/* ========================================================================
 * Bootstrap: dropdown.js v3.3.7
 * http://getbootstrap.com/javascript/#dropdowns
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // DROPDOWN CLASS DEFINITION
  // =========================

  var backdrop = '.dropdown-backdrop'
  var toggle   = '[data-toggle="dropdown"]'
  var Dropdown = function (element) {
    $(element).on('click.bs.dropdown', this.toggle)
  }

  Dropdown.VERSION = '3.3.7'

  function getParent($this) {
    var selector = $this.attr('data-target')

    if (!selector) {
      selector = $this.attr('href')
      selector = selector && /#[A-Za-z]/.test(selector) && selector.replace(/.*(?=#[^\s]*$)/, '') // strip for ie7
    }

    var $parent = selector && $(selector)

    return $parent && $parent.length ? $parent : $this.parent()
  }

  function clearMenus(e) {
    if (e && e.which === 3) return
    $(backdrop).remove()
    $(toggle).each(function () {
      var $this         = $(this)
      var $parent       = getParent($this)
      var relatedTarget = { relatedTarget: this }

      if (!$parent.hasClass('open')) return

      if (e && e.type == 'click' && /input|textarea/i.test(e.target.tagName) && $.contains($parent[0], e.target)) return

      $parent.trigger(e = $.Event('hide.bs.dropdown', relatedTarget))

      if (e.isDefaultPrevented()) return

      $this.attr('aria-expanded', 'false')
      $parent.removeClass('open').trigger($.Event('hidden.bs.dropdown', relatedTarget))
    })
  }

  Dropdown.prototype.toggle = function (e) {
    var $this = $(this)

    if ($this.is('.disabled, :disabled')) return

    var $parent  = getParent($this)
    var isActive = $parent.hasClass('open')

    clearMenus()

    if (!isActive) {
      if ('ontouchstart' in document.documentElement && !$parent.closest('.navbar-nav').length) {
        // if mobile we use a backdrop because click events don't delegate
        $(document.createElement('div'))
          .addClass('dropdown-backdrop')
          .insertAfter($(this))
          .on('click', clearMenus)
      }

      var relatedTarget = { relatedTarget: this }
      $parent.trigger(e = $.Event('show.bs.dropdown', relatedTarget))

      if (e.isDefaultPrevented()) return

      $this
        .trigger('focus')
        .attr('aria-expanded', 'true')

      $parent
        .toggleClass('open')
        .trigger($.Event('shown.bs.dropdown', relatedTarget))
    }

    return false
  }

  Dropdown.prototype.keydown = function (e) {
    if (!/(38|40|27|32)/.test(e.which) || /input|textarea/i.test(e.target.tagName)) return

    var $this = $(this)

    e.preventDefault()
    e.stopPropagation()

    if ($this.is('.disabled, :disabled')) return

    var $parent  = getParent($this)
    var isActive = $parent.hasClass('open')

    if (!isActive && e.which != 27 || isActive && e.which == 27) {
      if (e.which == 27) $parent.find(toggle).trigger('focus')
      return $this.trigger('click')
    }

    var desc = ' li:not(.disabled):visible a'
    var $items = $parent.find('.dropdown-menu' + desc)

    if (!$items.length) return

    var index = $items.index(e.target)

    if (e.which == 38 && index > 0)                 index--         // up
    if (e.which == 40 && index < $items.length - 1) index++         // down
    if (!~index)                                    index = 0

    $items.eq(index).trigger('focus')
  }


  // DROPDOWN PLUGIN DEFINITION
  // ==========================

  function Plugin(option) {
    return this.each(function () {
      var $this = $(this)
      var data  = $this.data('bs.dropdown')

      if (!data) $this.data('bs.dropdown', (data = new Dropdown(this)))
      if (typeof option == 'string') data[option].call($this)
    })
  }

  var old = $.fn.dropdown

  $.fn.dropdown             = Plugin
  $.fn.dropdown.Constructor = Dropdown


  // DROPDOWN NO CONFLICT
  // ====================

  $.fn.dropdown.noConflict = function () {
    $.fn.dropdown = old
    return this
  }


  // APPLY TO STANDARD DROPDOWN ELEMENTS
  // ===================================

  $(document)
    .on('click.bs.dropdown.data-api', clearMenus)
    .on('click.bs.dropdown.data-api', '.dropdown form', function (e) { e.stopPropagation() })
    .on('click.bs.dropdown.data-api', toggle, Dropdown.prototype.toggle)
    .on('keydown.bs.dropdown.data-api', toggle, Dropdown.prototype.keydown)
    .on('keydown.bs.dropdown.data-api', '.dropdown-menu', Dropdown.prototype.keydown)

}(jQuery);
;
/* ========================================================================
 * Bootstrap: tooltip.js v3.3.7
 * http://getbootstrap.com/javascript/#tooltip
 * Inspired by the original jQuery.tipsy by Jason Frame
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // TOOLTIP PUBLIC CLASS DEFINITION
  // ===============================

  var Tooltip = function (element, options) {
    this.type       = null
    this.options    = null
    this.enabled    = null
    this.timeout    = null
    this.hoverState = null
    this.$element   = null
    this.inState    = null

    this.init('tooltip', element, options)
  }

  Tooltip.VERSION  = '3.3.7'

  Tooltip.TRANSITION_DURATION = 150

  Tooltip.DEFAULTS = {
    animation: true,
    placement: 'top',
    selector: false,
    template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
    trigger: 'hover focus',
    title: '',
    delay: 0,
    html: false,
    container: false,
    viewport: {
      selector: 'body',
      padding: 0
    }
  }

  Tooltip.prototype.init = function (type, element, options) {
    this.enabled   = true
    this.type      = type
    this.$element  = $(element)
    this.options   = this.getOptions(options)
    this.$viewport = this.options.viewport && $($.isFunction(this.options.viewport) ? this.options.viewport.call(this, this.$element) : (this.options.viewport.selector || this.options.viewport))
    this.inState   = { click: false, hover: false, focus: false }

    if (this.$element[0] instanceof document.constructor && !this.options.selector) {
      throw new Error('`selector` option must be specified when initializing ' + this.type + ' on the window.document object!')
    }

    var triggers = this.options.trigger.split(' ')

    for (var i = triggers.length; i--;) {
      var trigger = triggers[i]

      if (trigger == 'click') {
        this.$element.on('click.' + this.type, this.options.selector, $.proxy(this.toggle, this))
      } else if (trigger != 'manual') {
        var eventIn  = trigger == 'hover' ? 'mouseenter' : 'focusin'
        var eventOut = trigger == 'hover' ? 'mouseleave' : 'focusout'

        this.$element.on(eventIn  + '.' + this.type, this.options.selector, $.proxy(this.enter, this))
        this.$element.on(eventOut + '.' + this.type, this.options.selector, $.proxy(this.leave, this))
      }
    }

    this.options.selector ?
      (this._options = $.extend({}, this.options, { trigger: 'manual', selector: '' })) :
      this.fixTitle()
  }

  Tooltip.prototype.getDefaults = function () {
    return Tooltip.DEFAULTS
  }

  Tooltip.prototype.getOptions = function (options) {
    options = $.extend({}, this.getDefaults(), this.$element.data(), options)

    if (options.delay && typeof options.delay == 'number') {
      options.delay = {
        show: options.delay,
        hide: options.delay
      }
    }

    return options
  }

  Tooltip.prototype.getDelegateOptions = function () {
    var options  = {}
    var defaults = this.getDefaults()

    this._options && $.each(this._options, function (key, value) {
      if (defaults[key] != value) options[key] = value
    })

    return options
  }

  Tooltip.prototype.enter = function (obj) {
    var self = obj instanceof this.constructor ?
      obj : $(obj.currentTarget).data('bs.' + this.type)

    if (!self) {
      self = new this.constructor(obj.currentTarget, this.getDelegateOptions())
      $(obj.currentTarget).data('bs.' + this.type, self)
    }

    if (obj instanceof $.Event) {
      self.inState[obj.type == 'focusin' ? 'focus' : 'hover'] = true
    }

    if (self.tip().hasClass('in') || self.hoverState == 'in') {
      self.hoverState = 'in'
      return
    }

    clearTimeout(self.timeout)

    self.hoverState = 'in'

    if (!self.options.delay || !self.options.delay.show) return self.show()

    self.timeout = setTimeout(function () {
      if (self.hoverState == 'in') self.show()
    }, self.options.delay.show)
  }

  Tooltip.prototype.isInStateTrue = function () {
    for (var key in this.inState) {
      if (this.inState[key]) return true
    }

    return false
  }

  Tooltip.prototype.leave = function (obj) {
    var self = obj instanceof this.constructor ?
      obj : $(obj.currentTarget).data('bs.' + this.type)

    if (!self) {
      self = new this.constructor(obj.currentTarget, this.getDelegateOptions())
      $(obj.currentTarget).data('bs.' + this.type, self)
    }

    if (obj instanceof $.Event) {
      self.inState[obj.type == 'focusout' ? 'focus' : 'hover'] = false
    }

    if (self.isInStateTrue()) return

    clearTimeout(self.timeout)

    self.hoverState = 'out'

    if (!self.options.delay || !self.options.delay.hide) return self.hide()

    self.timeout = setTimeout(function () {
      if (self.hoverState == 'out') self.hide()
    }, self.options.delay.hide)
  }

  Tooltip.prototype.show = function () {
    var e = $.Event('show.bs.' + this.type)

    if (this.hasContent() && this.enabled) {
      this.$element.trigger(e)

      var inDom = $.contains(this.$element[0].ownerDocument.documentElement, this.$element[0])
      if (e.isDefaultPrevented() || !inDom) return
      var that = this

      var $tip = this.tip()

      var tipId = this.getUID(this.type)

      this.setContent()
      $tip.attr('id', tipId)
      this.$element.attr('aria-describedby', tipId)

      if (this.options.animation) $tip.addClass('fade')

      var placement = typeof this.options.placement == 'function' ?
        this.options.placement.call(this, $tip[0], this.$element[0]) :
        this.options.placement

      var autoToken = /\s?auto?\s?/i
      var autoPlace = autoToken.test(placement)
      if (autoPlace) placement = placement.replace(autoToken, '') || 'top'

      $tip
        .detach()
        .css({ top: 0, left: 0, display: 'block' })
        .addClass(placement)
        .data('bs.' + this.type, this)

      this.options.container ? $tip.appendTo(this.options.container) : $tip.insertAfter(this.$element)
      this.$element.trigger('inserted.bs.' + this.type)

      var pos          = this.getPosition()
      var actualWidth  = $tip[0].offsetWidth
      var actualHeight = $tip[0].offsetHeight

      if (autoPlace) {
        var orgPlacement = placement
        var viewportDim = this.getPosition(this.$viewport)

        placement = placement == 'bottom' && pos.bottom + actualHeight > viewportDim.bottom ? 'top'    :
                    placement == 'top'    && pos.top    - actualHeight < viewportDim.top    ? 'bottom' :
                    placement == 'right'  && pos.right  + actualWidth  > viewportDim.width  ? 'left'   :
                    placement == 'left'   && pos.left   - actualWidth  < viewportDim.left   ? 'right'  :
                    placement

        $tip
          .removeClass(orgPlacement)
          .addClass(placement)
      }

      var calculatedOffset = this.getCalculatedOffset(placement, pos, actualWidth, actualHeight)

      this.applyPlacement(calculatedOffset, placement)

      var complete = function () {
        var prevHoverState = that.hoverState
        that.$element.trigger('shown.bs.' + that.type)
        that.hoverState = null

        if (prevHoverState == 'out') that.leave(that)
      }

      $.support.transition && this.$tip.hasClass('fade') ?
        $tip
          .one('bsTransitionEnd', complete)
          .emulateTransitionEnd(Tooltip.TRANSITION_DURATION) :
        complete()
    }
  }

  Tooltip.prototype.applyPlacement = function (offset, placement) {
    var $tip   = this.tip()
    var width  = $tip[0].offsetWidth
    var height = $tip[0].offsetHeight

    // manually read margins because getBoundingClientRect includes difference
    var marginTop = parseInt($tip.css('margin-top'), 10)
    var marginLeft = parseInt($tip.css('margin-left'), 10)

    // we must check for NaN for ie 8/9
    if (isNaN(marginTop))  marginTop  = 0
    if (isNaN(marginLeft)) marginLeft = 0

    offset.top  += marginTop
    offset.left += marginLeft

    // $.fn.offset doesn't round pixel values
    // so we use setOffset directly with our own function B-0
    $.offset.setOffset($tip[0], $.extend({
      using: function (props) {
        $tip.css({
          top: Math.round(props.top),
          left: Math.round(props.left)
        })
      }
    }, offset), 0)

    $tip.addClass('in')

    // check to see if placing tip in new offset caused the tip to resize itself
    var actualWidth  = $tip[0].offsetWidth
    var actualHeight = $tip[0].offsetHeight

    if (placement == 'top' && actualHeight != height) {
      offset.top = offset.top + height - actualHeight
    }

    var delta = this.getViewportAdjustedDelta(placement, offset, actualWidth, actualHeight)

    if (delta.left) offset.left += delta.left
    else offset.top += delta.top

    var isVertical          = /top|bottom/.test(placement)
    var arrowDelta          = isVertical ? delta.left * 2 - width + actualWidth : delta.top * 2 - height + actualHeight
    var arrowOffsetPosition = isVertical ? 'offsetWidth' : 'offsetHeight'

    $tip.offset(offset)
    this.replaceArrow(arrowDelta, $tip[0][arrowOffsetPosition], isVertical)
  }

  Tooltip.prototype.replaceArrow = function (delta, dimension, isVertical) {
    this.arrow()
      .css(isVertical ? 'left' : 'top', 50 * (1 - delta / dimension) + '%')
      .css(isVertical ? 'top' : 'left', '')
  }

  Tooltip.prototype.setContent = function () {
    var $tip  = this.tip()
    var title = this.getTitle()

    $tip.find('.tooltip-inner')[this.options.html ? 'html' : 'text'](title)
    $tip.removeClass('fade in top bottom left right')
  }

  Tooltip.prototype.hide = function (callback) {
    var that = this
    var $tip = $(this.$tip)
    var e    = $.Event('hide.bs.' + this.type)

    function complete() {
      if (that.hoverState != 'in') $tip.detach()
      if (that.$element) { // TODO: Check whether guarding this code with this `if` is really necessary.
        that.$element
          .removeAttr('aria-describedby')
          .trigger('hidden.bs.' + that.type)
      }
      callback && callback()
    }

    this.$element.trigger(e)

    if (e.isDefaultPrevented()) return

    $tip.removeClass('in')

    $.support.transition && $tip.hasClass('fade') ?
      $tip
        .one('bsTransitionEnd', complete)
        .emulateTransitionEnd(Tooltip.TRANSITION_DURATION) :
      complete()

    this.hoverState = null

    return this
  }

  Tooltip.prototype.fixTitle = function () {
    var $e = this.$element
    if ($e.attr('title') || typeof $e.attr('data-original-title') != 'string') {
      $e.attr('data-original-title', $e.attr('title') || '').attr('title', '')
    }
  }

  Tooltip.prototype.hasContent = function () {
    return this.getTitle()
  }

  Tooltip.prototype.getPosition = function ($element) {
    $element   = $element || this.$element

    var el     = $element[0]
    var isBody = el.tagName == 'BODY'

    var elRect    = el.getBoundingClientRect()
    if (elRect.width == null) {
      // width and height are missing in IE8, so compute them manually; see https://github.com/twbs/bootstrap/issues/14093
      elRect = $.extend({}, elRect, { width: elRect.right - elRect.left, height: elRect.bottom - elRect.top })
    }
    var isSvg = window.SVGElement && el instanceof window.SVGElement
    // Avoid using $.offset() on SVGs since it gives incorrect results in jQuery 3.
    // See https://github.com/twbs/bootstrap/issues/20280
    var elOffset  = isBody ? { top: 0, left: 0 } : (isSvg ? null : $element.offset())
    var scroll    = { scroll: isBody ? document.documentElement.scrollTop || document.body.scrollTop : $element.scrollTop() }
    var outerDims = isBody ? { width: $(window).width(), height: $(window).height() } : null

    return $.extend({}, elRect, scroll, outerDims, elOffset)
  }

  Tooltip.prototype.getCalculatedOffset = function (placement, pos, actualWidth, actualHeight) {
    return placement == 'bottom' ? { top: pos.top + pos.height,   left: pos.left + pos.width / 2 - actualWidth / 2 } :
           placement == 'top'    ? { top: pos.top - actualHeight, left: pos.left + pos.width / 2 - actualWidth / 2 } :
           placement == 'left'   ? { top: pos.top + pos.height / 2 - actualHeight / 2, left: pos.left - actualWidth } :
        /* placement == 'right' */ { top: pos.top + pos.height / 2 - actualHeight / 2, left: pos.left + pos.width }

  }

  Tooltip.prototype.getViewportAdjustedDelta = function (placement, pos, actualWidth, actualHeight) {
    var delta = { top: 0, left: 0 }
    if (!this.$viewport) return delta

    var viewportPadding = this.options.viewport && this.options.viewport.padding || 0
    var viewportDimensions = this.getPosition(this.$viewport)

    if (/right|left/.test(placement)) {
      var topEdgeOffset    = pos.top - viewportPadding - viewportDimensions.scroll
      var bottomEdgeOffset = pos.top + viewportPadding - viewportDimensions.scroll + actualHeight
      if (topEdgeOffset < viewportDimensions.top) { // top overflow
        delta.top = viewportDimensions.top - topEdgeOffset
      } else if (bottomEdgeOffset > viewportDimensions.top + viewportDimensions.height) { // bottom overflow
        delta.top = viewportDimensions.top + viewportDimensions.height - bottomEdgeOffset
      }
    } else {
      var leftEdgeOffset  = pos.left - viewportPadding
      var rightEdgeOffset = pos.left + viewportPadding + actualWidth
      if (leftEdgeOffset < viewportDimensions.left) { // left overflow
        delta.left = viewportDimensions.left - leftEdgeOffset
      } else if (rightEdgeOffset > viewportDimensions.right) { // right overflow
        delta.left = viewportDimensions.left + viewportDimensions.width - rightEdgeOffset
      }
    }

    return delta
  }

  Tooltip.prototype.getTitle = function () {
    var title
    var $e = this.$element
    var o  = this.options

    title = $e.attr('data-original-title')
      || (typeof o.title == 'function' ? o.title.call($e[0]) :  o.title)

    return title
  }

  Tooltip.prototype.getUID = function (prefix) {
    do prefix += ~~(Math.random() * 1000000)
    while (document.getElementById(prefix))
    return prefix
  }

  Tooltip.prototype.tip = function () {
    if (!this.$tip) {
      this.$tip = $(this.options.template)
      if (this.$tip.length != 1) {
        throw new Error(this.type + ' `template` option must consist of exactly 1 top-level element!')
      }
    }
    return this.$tip
  }

  Tooltip.prototype.arrow = function () {
    return (this.$arrow = this.$arrow || this.tip().find('.tooltip-arrow'))
  }

  Tooltip.prototype.enable = function () {
    this.enabled = true
  }

  Tooltip.prototype.disable = function () {
    this.enabled = false
  }

  Tooltip.prototype.toggleEnabled = function () {
    this.enabled = !this.enabled
  }

  Tooltip.prototype.toggle = function (e) {
    var self = this
    if (e) {
      self = $(e.currentTarget).data('bs.' + this.type)
      if (!self) {
        self = new this.constructor(e.currentTarget, this.getDelegateOptions())
        $(e.currentTarget).data('bs.' + this.type, self)
      }
    }

    if (e) {
      self.inState.click = !self.inState.click
      if (self.isInStateTrue()) self.enter(self)
      else self.leave(self)
    } else {
      self.tip().hasClass('in') ? self.leave(self) : self.enter(self)
    }
  }

  Tooltip.prototype.destroy = function () {
    var that = this
    clearTimeout(this.timeout)
    this.hide(function () {
      that.$element.off('.' + that.type).removeData('bs.' + that.type)
      if (that.$tip) {
        that.$tip.detach()
      }
      that.$tip = null
      that.$arrow = null
      that.$viewport = null
      that.$element = null
    })
  }


  // TOOLTIP PLUGIN DEFINITION
  // =========================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.tooltip')
      var options = typeof option == 'object' && option

      if (!data && /destroy|hide/.test(option)) return
      if (!data) $this.data('bs.tooltip', (data = new Tooltip(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.tooltip

  $.fn.tooltip             = Plugin
  $.fn.tooltip.Constructor = Tooltip


  // TOOLTIP NO CONFLICT
  // ===================

  $.fn.tooltip.noConflict = function () {
    $.fn.tooltip = old
    return this
  }

}(jQuery);
;
/* ========================================================================
 * Bootstrap: popover.js v3.3.7
 * http://getbootstrap.com/javascript/#popovers
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // POPOVER PUBLIC CLASS DEFINITION
  // ===============================

  var Popover = function (element, options) {
    this.init('popover', element, options)
  }

  if (!$.fn.tooltip) throw new Error('Popover requires tooltip.js')

  Popover.VERSION  = '3.3.7'

  Popover.DEFAULTS = $.extend({}, $.fn.tooltip.Constructor.DEFAULTS, {
    placement: 'right',
    trigger: 'click',
    content: '',
    template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
  })


  // NOTE: POPOVER EXTENDS tooltip.js
  // ================================

  Popover.prototype = $.extend({}, $.fn.tooltip.Constructor.prototype)

  Popover.prototype.constructor = Popover

  Popover.prototype.getDefaults = function () {
    return Popover.DEFAULTS
  }

  Popover.prototype.setContent = function () {
    var $tip    = this.tip()
    var title   = this.getTitle()
    var content = this.getContent()

    $tip.find('.popover-title')[this.options.html ? 'html' : 'text'](title)
    $tip.find('.popover-content').children().detach().end()[ // we use append for html objects to maintain js events
      this.options.html ? (typeof content == 'string' ? 'html' : 'append') : 'text'
    ](content)

    $tip.removeClass('fade top bottom left right in')

    // IE8 doesn't accept hiding via the `:empty` pseudo selector, we have to do
    // this manually by checking the contents.
    if (!$tip.find('.popover-title').html()) $tip.find('.popover-title').hide()
  }

  Popover.prototype.hasContent = function () {
    return this.getTitle() || this.getContent()
  }

  Popover.prototype.getContent = function () {
    var $e = this.$element
    var o  = this.options

    return $e.attr('data-content')
      || (typeof o.content == 'function' ?
            o.content.call($e[0]) :
            o.content)
  }

  Popover.prototype.arrow = function () {
    return (this.$arrow = this.$arrow || this.tip().find('.arrow'))
  }


  // POPOVER PLUGIN DEFINITION
  // =========================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.popover')
      var options = typeof option == 'object' && option

      if (!data && /destroy|hide/.test(option)) return
      if (!data) $this.data('bs.popover', (data = new Popover(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.popover

  $.fn.popover             = Plugin
  $.fn.popover.Constructor = Popover


  // POPOVER NO CONFLICT
  // ===================

  $.fn.popover.noConflict = function () {
    $.fn.popover = old
    return this
  }

}(jQuery);
;
/*!
 * hoverIntent v1.8.1 // 2014.08.11 // jQuery v1.9.1+
 * http://cherne.net/brian/resources/jquery.hoverIntent.html
 *
 * You may use hoverIntent under the terms of the MIT license. Basically that
 * means you are free to use hoverIntent as long as this header is left intact.
 * Copyright 2007, 2014 Brian Cherne
 */
 
/* hoverIntent is similar to jQuery's built-in "hover" method except that
 * instead of firing the handlerIn function immediately, hoverIntent checks
 * to see if the user's mouse has slowed down (beneath the sensitivity
 * threshold) before firing the event. The handlerOut function is only
 * called after a matching handlerIn.
 *
 * // basic usage ... just like .hover()
 * .hoverIntent( handlerIn, handlerOut )
 * .hoverIntent( handlerInOut )
 *
 * // basic usage ... with event delegation!
 * .hoverIntent( handlerIn, handlerOut, selector )
 * .hoverIntent( handlerInOut, selector )
 *
 * // using a basic configuration object
 * .hoverIntent( config )
 *
 * @param  handlerIn   function OR configuration object
 * @param  handlerOut  function OR selector for delegation OR undefined
 * @param  selector    selector OR undefined
 * @author Brian Cherne <brian(at)cherne(dot)net>
 */
(function($) {
    $.fn.hoverIntent = function(handlerIn,handlerOut,selector) {

        // default configuration values
        var cfg = {
            interval: 100,
            sensitivity: 6,
            timeout: 0
        };

        if ( typeof handlerIn === "object" ) {
            cfg = $.extend(cfg, handlerIn );
        } else if ($.isFunction(handlerOut)) {
            cfg = $.extend(cfg, { over: handlerIn, out: handlerOut, selector: selector } );
        } else {
            cfg = $.extend(cfg, { over: handlerIn, out: handlerIn, selector: handlerOut } );
        }

        // instantiate variables
        // cX, cY = current X and Y position of mouse, updated by mousemove event
        // pX, pY = previous X and Y position of mouse, set by mouseover and polling interval
        var cX, cY, pX, pY;

        // A private function for getting mouse position
        var track = function(ev) {
            cX = ev.pageX;
            cY = ev.pageY;
        };

        // A private function for comparing current and previous mouse position
        var compare = function(ev,ob) {
            ob.hoverIntent_t = clearTimeout(ob.hoverIntent_t);
            // compare mouse positions to see if they've crossed the threshold
            if ( Math.sqrt( (pX-cX)*(pX-cX) + (pY-cY)*(pY-cY) ) < cfg.sensitivity ) {
                $(ob).off("mousemove.hoverIntent",track);
                // set hoverIntent state to true (so mouseOut can be called)
                ob.hoverIntent_s = true;
                return cfg.over.apply(ob,[ev]);
            } else {
                // set previous coordinates for next time
                pX = cX; pY = cY;
                // use self-calling timeout, guarantees intervals are spaced out properly (avoids JavaScript timer bugs)
                ob.hoverIntent_t = setTimeout( function(){compare(ev, ob);} , cfg.interval );
            }
        };

        // A private function for delaying the mouseOut function
        var delay = function(ev,ob) {
            ob.hoverIntent_t = clearTimeout(ob.hoverIntent_t);
            ob.hoverIntent_s = false;
            return cfg.out.apply(ob,[ev]);
        };

        // A private function for handling mouse 'hovering'
        var handleHover = function(e) {
            // copy objects to be passed into t (required for event object to be passed in IE)
            var ev = $.extend({},e);
            var ob = this;

            // cancel hoverIntent timer if it exists
            if (ob.hoverIntent_t) { ob.hoverIntent_t = clearTimeout(ob.hoverIntent_t); }

            // if e.type === "mouseenter"
            if (e.type === "mouseenter") {
                // set "previous" X and Y position based on initial entry point
                pX = ev.pageX; pY = ev.pageY;
                // update "current" X and Y position based on mousemove
                $(ob).on("mousemove.hoverIntent",track);
                // start polling interval (self-calling timeout) to compare mouse coordinates over time
                if (!ob.hoverIntent_s) { ob.hoverIntent_t = setTimeout( function(){compare(ev,ob);} , cfg.interval );}

                // else e.type == "mouseleave"
            } else {
                // unbind expensive mousemove event
                $(ob).off("mousemove.hoverIntent",track);
                // if hoverIntent state is true, then call the mouseOut function after the specified delay
                if (ob.hoverIntent_s) { ob.hoverIntent_t = setTimeout( function(){delay(ev,ob);} , cfg.timeout );}
            }
        };

        // listen for mouseenter and mouseleave
        return this.on({'mouseenter.hoverIntent':handleHover,'mouseleave.hoverIntent':handleHover}, cfg.selector);
    };
})(jQuery);
;
/**
 * @file
 * Provides a 'mask' overlay.
 *
 * The core of this plugin is from jQuery Tools.
 * @see https://github.com/jquerytools/jquerytools/tree/master/src/toolbox.
 */

(function ($) {
  'use strict';

  var tool;

  tool = $.tool = {

    conf: {
      maskId: 'exposeMask',
      loadSpeed: 'slow',
      closeSpeed: 'fast',
      closeOnClick: true,
      closeOnEsc: true,

      // CSS settings.
      zIndex: 9998,
      opacity: 0.25,
      startOpacity: 0,
      color: '#333333',

      // Callbacks.
      onLoad: null,
      onClose: null
    }
  };

  function call(fn) {
    if (fn) {
      return fn.call($.mask);
    }
  }

  var mask;
  var loaded;
  var config;

  $.mask = {

    load: function (conf, els) {
      // Is the mask already loaded?
      if (loaded) {
        return this;
      }

      // Set configuration.
      if (typeof conf === 'string') {
        conf = {color: conf};
      }

      // Use latest config.
      conf = conf || config;

      config = conf = $.extend($.extend({}, tool.conf), conf);

      // Get the mask.
      mask = $('#' + conf.maskId);

      // Or create it.
      if (!mask.length) {
        mask = $('<div/>').attr('id', conf.maskId);
        $('body').append(mask);
      }

      // Set position and dimensions.
      mask.css({
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        display: 'none',
        opacity: conf.startOpacity,
        zIndex: conf.zIndex
      });

      if (conf.color) {
        mask.css('backgroundColor', conf.color);
      }

      // onBeforeLoad.
      if (call(conf.onBeforeLoad) === false) {
        return this;
      }

      // 'esc' button.
      if (conf.closeOnEsc) {
        $(document).on('keydown.mask', function (e) {
          if (parseInt(e.keyCode) === 27) {
            $.mask.close(e);
          }
        });
      }

      // Mask click closes.
      if (conf.closeOnClick) {
        mask.on('click.mask', function (e) {
          $.mask.close(e);
        });
      }

      // Reveal mask.
      mask.css({display: 'block'}).fadeTo(conf.loadSpeed, conf.opacity, function () {
        call(conf.onLoad);
        loaded = 'full';
      });

      loaded = true;
      return this;
    },

    close: function () {
      if (loaded) {

        // onBeforeClose.
        if (call(config.onBeforeClose) === false) {
          return this;
        }

        mask.fadeOut(config.closeSpeed, function () {
          call(config.onClose);
          loaded = false;
        });

        // Declare the mask closed.
        $(document).trigger('mask::closed');

        // Unbind various event listeners.
        $(document).off('keydown.mask');
        mask.off('click.mask');
      }

      return this;
    },

    getMask: function () {
      return mask;
    },

    isLoaded: function (fully) {
      return fully ? loaded === 'full' : loaded;
    },

    getConf: function () {
      return config;
    }
  };

  $(window).on('mask::open', function (event) {
    $(document).mask();
  });

  $(window).on('mask::close', function (event) {
    $(document).mask.close();
  });

  $.fn.mask = function (conf) {
    $.mask.load(conf);
    return this;
  };

  $.fn.mask.isLoaded = function () {
    $.mask.isLoaded();
    return this;
  };

  $.fn.mask.close = function () {
    $.mask.close();
    return this;
  };

  $.fn.mask.getMask = function () {
    $.mask.getMask();
    return this;
  };

})(jQuery);
;
/**
 * @file
 * Behaviours for Secondary menu.
 *
 * Note that dropdown behaviours are handled by Bootstrap functionality.
 *
 * Read about how menu is built in _header.secondary-navigation.scss.
 */

/* global jQuery, Drupal */

(function ($) {
  'use strict';

  Drupal.behaviors.victorySecondaryMenu = {
    SELECTOR_DROPDOWN_TRIGGER: '.level-2',
    SELECTOR_MENU: '.menu',
    SELECTOR_MENU_WRAPPER: '.menu-wrapper',
    SELECTOR_ACTIVE_TRAIL: '.active-trail',
    SELECTOR_MENU_LEAF: '.leaf',
    SELECTOR_MENU_LEAF_HOVER: '.js-mouse-over',
    SELECTOR_MENU_LEAF_HOVER_START: '.js-mouse-over-start',
    SELECTOR_MENU_SECTION: '.section',
    SELECTOR_USE_HEIGHT: '.js-use-height',
    DURATION_OPEN: 720,
    DURATION_CLOSE: 200,
    HOVERINTENT_TIMEOUT: 300,
    attach: function (context, settings) {
      // Do not attach to AJAX responses.
      if (context !== window.document) {
        return;
      }
      // Explicitly specify container as parent with pre-defined selectors.
      var $dropdownTriggers = this.getChildMenu($('.region-navigation .menu-level-2', context)).find('>' + this.SELECTOR_DROPDOWN_TRIGGER);
      if ($dropdownTriggers.length === 0) {
        return;
      }

      // Init close button.
      this.initCloseButton($dropdownTriggers);

      // Init overlay integration.
      this.initOverlay($dropdownTriggers);

      // Get the tallest menu among all visible menus, revealed by mouse over.
      this.initDropdownMenu($dropdownTriggers);
    },

    /**
     * Init close button.
     *
     * @param {object} $dropdownTriggers
     *   Dropdown trigger elements.
     */
    initCloseButton: function ($dropdownTriggers) {
      var $menu = this.getChildMenu($dropdownTriggers);
      $menu.addClass('js-close-target');
      $(Drupal.theme('victorySecondaryMenuCloseButton', 'js-close-target')).appendTo($menu);
    },

    /**
     * Init overlay integration.
     *
     * @param {object} $dropdownTriggers
     *   Dropdown trigger elements.
     */
    initOverlay: function ($dropdownTriggers) {
      // Show the overlay when the dropdown is start to open and remove it when
      // it is closed, but track last clicked element to make sure that overlay
      // is closed only when current item (or close button) is clicked.
      var lastClicked = null;
      $dropdownTriggers
        .on('click focus', function (evt) {
          lastClicked = evt.currentTarget;
          $('div.sticky-header').addClass('sticky-header--hidden');
        })
        .on('shown.bs.dropdown', function () {
          $dropdownTriggers.mask({
            maskId: 'mainMenuOverlay',
            zIndex: 200,
            onClose: function () {
              this.getMask().remove();
              $('div.sticky-header').removeClass('sticky-header--hidden');
            }
          });
        })
        .on('hide.bs.dropdown', function (evt) {
          if (lastClicked === evt.currentTarget) {
            $(document).mask.close();
          }
        });
    },

    /**
     * Init dropdown behaviours.
     *
     * @param {object} $dropdownTriggers
     *   Dropdown trigger elements.
     */
    initDropdownMenu: function ($dropdownTriggers) {
      var self = this;

      // Change the event that we're binding to to 'show' because 'shown' does
      // not correctly pass the relatedTarget property of the event.
      $dropdownTriggers.on('show.bs.dropdown', function (evt) {
        var $dropdown = self.getChildMenuWrapper($(evt.target));

        // Initiate default open state, but only when the dropdown is fully
        // opened.
        $dropdown.one('bsTransitionEnd', $.proxy(function () {
          var $currentDropdown = $(this);

          // Set default dropdown state and show active trail items.
          self.setDefaultDropdownState($currentDropdown);
          self.processActiveTrail($currentDropdown);

          // Reset dropdown state on mouseleave.
          $currentDropdown.on('mouseleave', function () {
            self.setDefaultDropdownState($(this));
            self.removeAllStartHoverStates($(this));
          });
        }, $dropdown)).emulateTransitionEnd(self.DURATION_OPEN / 2);

        // Reset hover stack every time dropdown opens.
        self.resetHoverStack($dropdown);

        // Bind setting of the menu heights on mouse move and leave.
        var $links = $dropdown.find('a').not('.js-processed').addClass('js-processed');
        $links.each(function () {
          // Bind hoverintent to each leaf, but sections (see below).
          // We are binding to leaves instead of links as leaves are used for
          // expanding children menus.
          self.getLeaf($(this)).filter(':not(' + self.SELECTOR_MENU_SECTION + ')').hoverIntent({
            // Set interval during which a cursor outside of an item is
            // considered still inside. If a mouse returns to the element
            // within this interval, neither 'out' nor 'over' callbacks are
            // called again.
            timeout: self.HOVERINTENT_TIMEOUT,
            over: function () {
              var $leaf = $(this);
              var $link = self.getLink($leaf);
              var $currentDropdown = self.getCurrentDropdown($link);
              // Since hoverInit does not provide an ability to cancel
              // event bubbling, we have to maintain our own stack of
              // hover events for currently hovered item to prevent bubbling to
              // parent items (i.e. when a child is hovered, we do not need
              // hover event to fire for parents).
              if (self.matchesHoverStack($currentDropdown, $leaf)) {
                return;
              }

              // Add current leaf to the hover stack.
              self.addToHoverStack($dropdown, $leaf);

              self.removeAllStartHoverStates($currentDropdown);

              // Add hover state.
              self.addHoverState($link);

              // Set height for current link.
              self.setCurrentDropdownHeight($link);
            },
            out: function () {
              var $leaf = $(this);
              var $link = self.getLink($leaf);
              var $currentDropdown = self.getCurrentDropdown($link);

              // Remove hover state.
              self.removeHoverState($link);
              self.resetHoverStack($currentDropdown);
            }
          });

          // Special treatment for section links.
          self.getLeaf($(this)).filter(self.SELECTOR_MENU_SECTION).find('a:first').on('mouseenter', function () {
            var $link = $(this);
            var $currentDropdown = self.getCurrentDropdown($link);

            self.removeAllStartHoverStates($currentDropdown);

            // Add hover state.
            self.addHoverState($link);

            // Set height for current link.
            self.setCurrentDropdownHeight($link);
          }).on('mouseleave', function () {
            var $link = $(this);
            var $currentDropdown = self.getCurrentDropdown($link);

            // Remove hover state.
            self.removeHoverState($link);
            self.resetHoverStack($currentDropdown);
          });
        });
      }).on('hide.bs.dropdown', function (evt) {
        var $dropdown = self.getChildMenuWrapper($(evt.target));
        // Unbind all events.
        $dropdown.off('mouseleave');
        $dropdown.find('a.js-processed').removeClass('js-processed').off('mouseenter mouseleave');

        // Reset heights for all dropdowns.
        self.resetAllDropdownHeights($dropdown);
      });
    },

    /**
     * Get link to leaf.
     */
    getLink: function ($leaf) {
      return $leaf.find('>a:first');
    },

    /**
     * Get leaf from link.
     */
    getLeaf: function ($link) {
      return $link.parents('li:first');
    },

    /**
     * Add hover state for a link.
     */
    addHoverState: function ($link) {
      var $leaf = this.getLeaf($link);
      $leaf.parents('li').not(this.SELECTOR_DROPDOWN_TRIGGER).addClass(this.SELECTOR_MENU_LEAF_HOVER.substring(1));
      $leaf.addClass(this.SELECTOR_MENU_LEAF_HOVER.substring(1));
    },

    /**
     * Remove hover state from a link.
     */
    removeHoverState: function ($link) {
      var $leaf = this.getLeaf($link);
      $leaf.removeClass(this.SELECTOR_MENU_LEAF_HOVER.substring(1));
      // Remove from all children as well.
      $leaf.find('li' + this.SELECTOR_MENU_LEAF_HOVER).removeClass(this.SELECTOR_MENU_LEAF_HOVER.substring(1));
    },

    /**
     * Add starting hover state for a link.
     */
    addStartHoverState: function ($link) {
      this.getLeaf($link).not(this.SELECTOR_MENU_LEAF_HOVER_START).addClass(this.SELECTOR_MENU_LEAF_HOVER_START.substr(1));
    },

    /**
     * Remove all starting hover states for dropdown.
     */
    removeAllStartHoverStates: function ($dropdown) {
      $dropdown.find(this.SELECTOR_MENU_LEAF_HOVER_START).removeClass(this.SELECTOR_MENU_LEAF_HOVER_START.substr(1));
    },

    /**
     * Set default dropdown state.
     */
    setDefaultDropdownState: function ($dropdown) {
      var $defaultLink = $dropdown.find('a:first');
      this.setCurrentDropdownHeight($defaultLink);
    },

    /**
     * Add leaf to a hover stack for current dropdown.
     */
    addToHoverStack: function ($dropdown, $leaf) {
      var stack = this.getHoverStack($dropdown);
      stack.push($leaf);
      $dropdown.data('hoverStack', stack);
      return stack;
    },

    /**
     * Get hover stack for current dropdown.
     */
    getHoverStack: function ($dropdown) {
      var stack = $dropdown.data('hoverStack');
      return stack || [];
    },

    /**
     * Reset hover stack for current dropdown.
     */
    resetHoverStack: function ($dropdown) {
      $dropdown.data('hoverStack', []);
    },

    /**
     * Match provided leaf to elements in hover stack for current dropdown.
     *
     * Match is based on special criteria, not simple existence in stack.
     */
    matchesHoverStack: function ($dropdown, $leaf) {
      var stack = this.getHoverStack($dropdown);
      for (var i in stack) {
        // Match found if current leaf is a parent of an item in the stack
        // or already in the stack.
        if ($leaf.is(stack[i]) || stack[i].parents().filter($leaf).length > 0) {
          return true;
        }
      }
      return false;
    },

    /**
     * Process active trail links for the dropdown.
     */
    processActiveTrail: function ($dropdown) {
      var self = this;

      var $links = $dropdown.find('a').filter(this.SELECTOR_ACTIVE_TRAIL);

      // Add start hover state.
      $links.each(function () {
        self.addStartHoverState($(this));
      });

      // Set height to the last item of the active trail.
      var $lastLink = $links.last();
      if ($lastLink.length > 0) {
        self.setCurrentDropdownHeight($lastLink);
      }
    },

    /**
     * Set dropdown height for a specified link.
     */
    setCurrentDropdownHeight: function ($link) {
      var self = this;
      var $currentDropdown = self.getCurrentDropdown($link);

      // Calculate max height for the visible parents.
      var $parents = self.getCurrentMenus($link);
      var maxHeightParents = self.calcMaxHeight($parents);

      // Calculate max height for the visible immediate children.
      var $children = this.getChildMenu($link.parent()).filter(function () {
        return $(this).css('visibility') !== 'hidden';
      });
      var maxHeightChildren = self.calcMaxHeight($children);

      // Calculate max height for all other elements that have special class.
      var $extras = $currentDropdown.find(self.SELECTOR_USE_HEIGHT);
      var maxHeightExtras = self.calcMaxHeight($extras);

      // Set the height of the current dropdown to the max height of parents and
      // children menus.
      self.setDropdownHeight($currentDropdown, Math.max(maxHeightParents, maxHeightChildren, maxHeightExtras));
    },

    /**
     * Reset heights of all dropdowns.
     */
    resetAllDropdownHeights: function ($element) {
      var self = this;
      $element.parents(this.SELECTOR_MENU).first().find('>' + this.SELECTOR_DROPDOWN_TRIGGER).each(function () {
        self.getChildMenuWrapper($(this)).removeAttr('style');
      });
    },

    /**
     * Return current menu for a specified link.
     */
    getCurrentMenu: function ($link) {
      return this.getCurrentMenus($link).first();
    },

    /**
     * Return current and all parent menus for a specified link.
     */
    getCurrentMenus: function ($link) {
      return $link.parents(this.SELECTOR_MENU + ':visible').not(':last');
    },

    /**
     * Return current dropdown for a specified link.
     */
    getCurrentDropdown: function ($link) {
      return this.getChildMenuWrapper($link.parents(this.SELECTOR_DROPDOWN_TRIGGER));
    },

    /**
     * Return immediate child menu for a specified element.
     */
    getChildMenu: function ($element) {
      return $element.find('>' + this.SELECTOR_MENU_WRAPPER + '>' + this.SELECTOR_MENU);
    },

    /**
     * Return immediate child menu wrapper for a specified element.
     */
    getChildMenuWrapper: function ($element) {
      return this.getChildMenu($element).parents(this.SELECTOR_MENU_WRAPPER).first();
    },

    /**
     * Set dropdown height.
     */
    setDropdownHeight: function ($dropdown, height) {
      $dropdown.css('min-height', height);
    },

    /**
     * Calculate max height for a set of provided elements.
     */
    calcMaxHeight: function ($elements) {
      var maxHeight = 0;
      $elements.each(function () {
        var height = $(this).outerHeight(true);
        if (height > maxHeight) {
          maxHeight = height;
        }
      });
      return maxHeight;
    }
  };

  /**
   * Render the close button.
   *
   * @param {string} target
   *   Close target query.
   *
   * @returns {string}
   *   The markup for the toggle button.
   */
  Drupal.theme.prototype.victorySecondaryMenuCloseButton = function (target) {
    return '<button type="button" class="close" data-target="' + target + '" data-dismiss="alert" role="button">' +
      '<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>' +
      '</button>';
  };

}(jQuery, Drupal));
;
/**
 * @file
 * Mobile menu.
 *
 * Heads up! This file uses plugin-like architecture for decoupled binding
 * with Drupal.behaviors.victoryMobileMenu.attach() as a centralised plugin
 * initialisation method.
 * Also, some elements are created using JS theme implementations.
 */

/* global jQuery, Drupal */

(function ($, Drupal) {
  'use strict';
  Drupal.victory = Drupal.victory || {};

  /**
   * Behaviours for responsive mobile menu.
   */
  Drupal.behaviors.victoryMobileMenu = {
    attach: function (context) {
      // Do not attach to AJAX responses.
      if (context !== window.document) {
        return;
      }
      // Container for menu to append to.
      var $menuContainer = $('body', context);
      // Anchor to render menu trigger.
      var $menuTriggerAnchor = $('.js-responsive-menu-trigger-anchor', context);

      // Make mobile header sticky.
      Drupal.victory.fixedBlock.init($('.js-fixed-mobile-header', context), {
        classFixed: 'fixed-mobile-header',
        classFixedContainer: 'with-fixed-mobile-header'
      });

      // Create mobile menu.
      var $nav = Drupal.victory.mobileMenu.init({
        context: context,
        linksSelectorsPrimaryMenu: [
          'header .menu-block-main-menu-level1 .menu'
        ],
        linksSelectorsPrimaryMenuIgnoreItems: [
          '.js-responsive-menu-ignore'
        ],
        linksSelectorsSecondaryMenu: [
          '#page-header .menu-block-main-menu-level2'
        ],
        linksSelectorsSecondaryMenuIgnoreItems: [
          '.section',
          '.close'
        ],
        linksSelectorsDropdownTrigger: [
          'header .menu-block-main-menu-level1 .menu li .js-menu-item-login'
        ],
        linksSelectorsDropdownItems: [
          '#block-menu-block-main-menu-tools .menu li a'
        ],
        $container: $menuContainer
      });

      // Add off-canvas functionality for the menu.
      Drupal.victory.offCanvas.init({
        $container: $menuContainer,
        $triggerOpen: $(Drupal.theme('victoryOffcanvasTrigger', 'open', 'Menu')).insertAfter($menuTriggerAnchor, context),
        $triggerClose: $(Drupal.theme('victoryOffcanvasTrigger', 'close', 'Close')).appendTo($nav),
        $canvas: $menuContainer.children().filter('div, header, footer').not('.js-shutter'),
        $right: $nav
      });
    }
  };

  /**
   * Plugin to generate mobile menu.
   */
  Drupal.victory.mobileMenu = {
    SELECTOR_SLIDER_ROOT: '.js-menu-root',
    SELECTOR_LEAF: '.leaf, .expanded',
    SELECTOR_LEAF_EXPANDED: '.expanded',
    SELECTOR_ACTIVE_TRAIL: '.active, .active-trail',
    SELECTOR_MENU_EXPANDED: '.js-menu-expanded',
    SELECTOR_MENU_WRAPPER: '.menu-wrapper',
    SELECTOR_LINK_FORWARD: '.js-forward-link',
    SELECTOR_LINK_BACK: '.js-back-link',
    SELECTOR_LINK_WRAPPER: '.js-link-wrapper',
    SELECTOR_LINK_PARENT_WRAPPER: '.js-parent-link-wrapper',
    SELECTOR_SKIP_ANIMATION: '.js-skip-animation',
    SELECTOR_DEPTH_PREFIX: 'depth-',
    SELECTOR_LEVEL_PREFIX: 'level-',
    init: function (config) {
      var self = this;

      self.config = config;

      // Clone menu from existing primary and secondary menus.
      var $primary = $(self.config.linksSelectorsPrimaryMenu.join(','), self.config.context).clone();
      var $secondary = $(self.config.linksSelectorsSecondaryMenu.join(',') + '>' + self.SELECTOR_MENU_WRAPPER, self.config.context).clone();

      // Remove all ignored items from primary menu.
      $primary.find(self.config.linksSelectorsPrimaryMenuIgnoreItems.join(',')).parent().remove();
      // Remove all non-leaf items that are added for dropdown rendering.
      $secondary.find(self.config.linksSelectorsSecondaryMenuIgnoreItems.join(',')).remove();

      $primary.find(self.selectorToQuery(self.SELECTOR_ACTIVE_TRAIL, 'li')).addClass(self.SELECTOR_SLIDER_ROOT.substr(1)).append($secondary);

      // Add forward, parent and back links.
      $secondary.find(self.selectorToQuery(self.SELECTOR_LEAF, '', '>a')).each(function () {
        var $link = $(this);
        var $leaf = self.getLeaf($link);

        // Remove all data attributes that may have been assigned buy other
        // plugins from links.
        $.each($link.data(), function (idx) {
          $link.removeAttr('data-' + idx);
        });

        // Apply required markup to link itself to allow styling.
        $link.wrap(Drupal.theme('victoryMobileMenuLinkWrapper', $link));

        if (self.isLeafExpanded($leaf)) {
          // Inject current link as a parent for a child menu wrapper.
          self.getChildMenuWrapper($link).prepend(Drupal.theme('victoryMobileMenuParentLink', $link));

          // Inject parent link into child menu as a back button.
          var $parentLink = self.getParentLink($link);
          $parentLink = Drupal.theme('victoryMobileMenuBackLink', $parentLink);
          self.getChildMenuWrapper($link).prepend($parentLink);
          $parentLink.on('click', function () {
            self.hideSubmenu($(this));
          });

          // Add forward link.
          var $forwardLink = $(Drupal.theme('victoryMobileMenuForwardLink'));
          $link.after($forwardLink);
          $forwardLink.on('click', function () {
            self.showSubmenu($(this));
          });
        }
      });

      // Render nav.
      var $nav = $(Drupal.theme('victoryMobileMenuNav', $primary)).appendTo(config.$container);

      self.showActiveTrail($primary);

      // Re-run active trail once more to make sure that the longest menus
      // have the time to expand.
      setTimeout(function () {
        self.showActiveTrail($primary);
      }, 720);

      // Add dropdown menu, but only if both trigger and items exist.
      if ($(self.config.linksSelectorsDropdownTrigger).length > 0 && $(self.config.linksSelectorsDropdownItems).length > 0) {
        var linkDropdownTrigger = self.extractLinks(self.config.linksSelectorsDropdownTrigger);
        var linksDropdownItems = self.extractLinks(self.config.linksSelectorsDropdownItems);

        if (linksDropdownItems.length > 0) {
          // Render dropdown.
          $nav.prepend($(Drupal.theme('victoryMobileMenuDropdown', $(linkDropdownTrigger).get(0), self.linksToMenu(linksDropdownItems, self.SELECTOR_ACTIVE_TRAIL))));
        }
      }

      return $nav;
    },

    /**
     * Show submenu of the specified link.
     */
    showSubmenu: function ($link, skipAnimation) {
      var $leaf = this.getLeaf($link);
      $leaf.addClass(this.SELECTOR_MENU_EXPANDED.substr(1));
      if (skipAnimation) {
        $leaf.addClass(this.SELECTOR_SKIP_ANIMATION.substr(1));
      }
      this.getRootContainer($link).addClass(this.SELECTOR_DEPTH_PREFIX + this.getDepth($leaf));
      this.updateRootContainerHeight($link);
      if (skipAnimation) {
        $leaf.removeClass(this.SELECTOR_SKIP_ANIMATION.substr(1));
      }
    },

    /**
     * Hide submenu of the specified link.
     */
    hideSubmenu: function ($link) {
      var $leaf = this.getLeaf($link);
      $leaf.removeClass(this.SELECTOR_MENU_EXPANDED.substr(1));
      this.getRootContainer($link).removeClass(this.SELECTOR_DEPTH_PREFIX + this.getDepth($leaf));
      this.updateRootContainerHeight(this.getParentLink($link));
    },

    /**
     * Update root container height for specified link.
     */
    updateRootContainerHeight: function ($link) {
      var $menuWrapper = this.getChildMenuWrapper($link);
      if ($menuWrapper.is(':visible')) {
        var $rootContainer = this.getRootContainer($link);
        var $rootContainerLink = $rootContainer.find('>a');
        $rootContainer.height($menuWrapper.height() + $rootContainerLink.outerHeight(true));
      }
    },

    /**
     * Show active trail items for specified menu.
     */
    showActiveTrail: function ($menu) {
      var self = this;
      var $lastActiveTrail = $($menu.find(self.selectorToQuery(self.SELECTOR_LEAF)).filter(self.selectorToQuery('.active-trail')));
      $lastActiveTrail.each(function () {
        var $link = self.getLink($(this));
        // Show a leaf with children the same way as one of it's children (i.e.
        // leaf with children will be shown as a parent on the same slide
        // as it's childless children).
        $link = $(this).hasClass('leaf') ? self.getParentLink($link) : $link;
        self.showSubmenu($link, true);
      });
    },

    /**
     * Return current back link.
     */
    getCurrentBackLink: function ($menu) {
      return $menu.find(this.SELECTOR_MENU_EXPANDED + ':last').find(' > ' + self.SELECTOR_MENU_WRAPPER + ' > ' + this.SELECTOR_LINK_BACK);
    },

    /**
     * Get depth of specified leaf.
     */
    getDepth: function ($leaf) {
      var depth = 1;
      if ($leaf.length > 0) {
        var classes = $leaf.attr('class').split(' ').reverse();
        for (var i in classes) {
          if (classes[i].indexOf(this.SELECTOR_LEVEL_PREFIX) === 0) {
            depth = classes[i].substr(this.SELECTOR_LEVEL_PREFIX.length);
            break;
          }
        }
      }

      return depth;
    },

    /**
     * Get root container for specified link.
     */
    getRootContainer: function ($link) {
      return $link.parents(this.SELECTOR_SLIDER_ROOT + ':first');
    },

    /**
     * Get child menu wrapper for specified link.
     */
    getChildMenuWrapper: function ($link) {
      return this.getLeaf($link).find(this.SELECTOR_MENU_WRAPPER + ':first');
    },

    /**
     * Get link for leaf.
     */
    getLink: function ($leaf) {
      return $leaf.find(this.SELECTOR_LINK_WRAPPER + ' > a');
    },

    /**
     * Get leaf from link or another leaf element.
     */
    getLeaf: function ($el) {
      return $el.parents(this.selectorToQuery(this.SELECTOR_LEAF, '', ':first')).first();
    },

    /**
     * Check if provided leaf is expanded.
     */
    isLeafExpanded: function ($leaf) {
      return $leaf.is(this.SELECTOR_LEAF_EXPANDED);
    },

    /**
     * Get parent link for current link.
     */
    getParentLink: function ($link) {
      var $currentLeaf = this.getLeaf($link);
      var $parentLeaf = this.getLeaf($currentLeaf);
      return $parentLeaf.find('a:first');
    },

    /**
     * Extract links from specified selectors.
     *
     * @param {array} linksSelectors
     *   Array of jQuery selectors to extract links from.
     * @param {array|string} filterClasses
     *   Array or string of filter classes. If no classes provided - no
     *   filtering is used.
     *
     * @returns {Array}
     *   Array of extracted links.
     */
    extractLinks: function (linksSelectors, filterClasses) {
      filterClasses = filterClasses || false;
      var self = this;
      var links = [];
      for (var i = 0; i < linksSelectors.length; i++) {
        $(linksSelectors[i]).each(function () {
          var href = $(this).attr('href') ? $(this).attr('href') : '#';
          var cssClasses = !filterClasses || $(this).is(self.selectorToQuery(filterClasses)) ? $(this).attr('class') : '';
          links.push('<a href="' + href + '"' + (cssClasses !== '' ? ' class="' + cssClasses + '"' : '') + '>' + $(this).html() + '</a>');
        });
      }
      return links;
    },

    /**
     * Convert links to menu.
     *
     * @param {array} links
     *   Array of links.
     * @param {array|string} filterClasses
     *   Array or string of filter classes. If no classes provided - no
     *   filtering is used.
     *
     * @returns {string}
     *   HTML for menu list.
     */
    linksToMenu: function (links, filterClasses) {
      filterClasses = filterClasses || false;
      var self = this;
      var $ul = $('<ul></ul>');
      for (var i = 0; i < links.length; i++) {
        var cssClasses = !filterClasses || $(this).is(self.selectorToQuery(filterClasses)) ? $(this).attr('class') : '';
        $ul.append('<li' + (cssClasses !== '' ? ' class="' + cssClasses + '"' : '') + '>' + links[i] + '</li>');
      }
      return $ul.outerHTML();
    },

    /**
     * Convert selector to CSS query string.
     *
     * @param {string|array} classes
     *   Array or comma-separated list of classes.
     * @param {string} prefixQuery
     *   Optional prefix query string.
     * @param {string} suffixQuery
     *   Optional suffix query string.
     *
     * @returns {string}
     *   CSS query string.
     */
    selectorToQuery: function (classes, prefixQuery, suffixQuery) {
      classes = $.isArray(classes) ? classes : classes.split(',').map(function (classSelector) {
        return classSelector.trim().substr(1);
      });
      prefixQuery = prefixQuery || '';
      suffixQuery = suffixQuery || '';
      return classes.map(function (selector) {
        return prefixQuery + '.' + selector.trim() + suffixQuery;
      }).join(',');
    }
  };

  /**
   * Plugin for off-canvas functionality.
   */
  Drupal.victory.offCanvas = {

    /**
     * Init off-canvas behaviour.
     */
    init: function (config) {
      var self = this;

      if (config.$right.length === 0) {
        return;
      }

      // Add all classes for styling.
      config.$container.addClass('js-offcanvas-container');
      config.$triggerOpen.addClass('js-offcanvas-trigger-open');
      config.$triggerClose.addClass('js-offcanvas-trigger-close');
      config.$canvas.addClass('js-offcanvas-canvas');
      config.$right.addClass('js-offcanvas-right');

      $(config.$triggerOpen).on('click', function () {
        if (!config.$container.hasClass('js-offcanvas-open')) {
          config.$container.mask({
            maskId: 'responsiveMenuOverlay',
            loadSpeed: 0,
            closeSpeed: 100,
            onBeforeLoad: function () {
              // Overlay needs to be a part of sliding canvas.
              this.getMask().addClass('js-offcanvas-canvas');

              this.getMask().on('touchstart.offcanvas touchmove.offcanvas', function (e) {
                e.stopPropagation();
              });
            },
            onLoad: function () {
              // Fix for i-devices.
              if (self.isIDevice()) {
                var lastScroll = $(window).scrollTop();
                config.$container.addClass('js-offcanvas-open-idevice');
                config.$container.css('margin-top', -1 * lastScroll);
              }
              config.$container.addClass('js-offcanvas-open');
            },
            onClose: function () {
              config.$container.removeClass('js-offcanvas-open');
              this.getMask().off('touchstart.offcanvas touchmove.offcanvas');
              this.getMask().remove();

              // Fix for i-devices.
              if (self.isIDevice()) {
                var lastScroll = config.$container.css('margin-top');
                config.$container.css('margin-top', '0');
                if (lastScroll) {
                  window.scrollTo(0, -1 * parseInt(lastScroll, 10));
                }
                config.$container.removeClass('js-offcanvas-open-idevice');
              }
            }
          });
        }
      });

      $(config.$triggerClose).on('click', function () {
        if (config.$container.hasClass('js-offcanvas-open')) {
          config.$container.trigger('mask::close');
        }
      });
    },

    /**
     * Check that current device is iPhone or iPad.
     */
    isIDevice: function () {
      return navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/iPad/i);
    }
  };

  /**
   * Plugin to vertically fix block (make it sticky).
   */
  Drupal.victory.fixedBlock = {
    $element: $(),
    $siblings: $(),
    $container: $(),
    lastScroll: 0,
    config: {
      // Selector fo container of the block.
      selectorContainer: 'body',
      // Class to add when the block is fixed.
      classFixed: 'block-fixed',
      // Class to add to the container when the block is fixed.
      classFixedContainer: 'with-block-fixed'
    },

    /**
     * Init plugin.
     */
    init: function ($el, config) {
      var self = this;

      self.config = $.extend({}, self.config, config);

      self.$element = $el;
      self.$siblings = self.getSiblings(self.$element);
      self.$container = $(self.config.selectorContainer);

      // Trigger update if one of the siblings has an accessibility element that
      // receives a focus.
      $(document).on('focus.sr-group-item', function (evt, element) {
        if ($(element).parents().filter(self.$siblings).length > 0) {
          self.fixedBlockAnimationCallback();
          // Force update as scrolling may not occur.
          self.doUpdate();
        }
      });

      self.fixedBlockAnimationCallback();
      self.doUpdate();
    },

    /**
     * Get all siblings for currently active block element.
     */
    getSiblings: function ($el) {
      var $siblings = $();
      var $allSiblings = $('[data-fixed-block-target]', document);
      $allSiblings.each(function () {
        var selector = $(this).data('fixedBlockTarget');
        if ($(selector).is($el)) {
          $siblings = $siblings.add($(this));
        }
      });
      return $siblings;
    },

    /**
     * Sticky animation animation callback handler.
     *
     * This handler automatically loops on animframe as soon as it's invoked.
     */
    fixedBlockAnimationCallback: function () {
      var self = Drupal.victory.fixedBlock;
      window.requestAnimFrame(self.fixedBlockAnimationCallback);

      var scrollTop = $(window).scrollTop();

      // Don't advance if no scrolling has occurred.
      if (self.lastScroll === scrollTop) {
        return;
      }

      // Compensate for elastic scrolling.
      if (scrollTop < 0) {
        scrollTop = 0;
      }

      // Store last scroll position.
      Drupal.victory.fixedBlock.lastScroll = scrollTop;

      // Perform actual update.
      self.doUpdate();
    },

    /**
     * Perform block update.
     */
    doUpdate: function () {
      var self = Drupal.victory.fixedBlock;

      // Get the max vertical offset for the siblings with non-zero height.
      var siblingsTop = 0;
      self.$siblings.each(function () {
        var h = $(this).outerHeight(true);
        if (h > 0) {
          siblingsTop = Math.max(siblingsTop, h);
        }
      });

      if (self.lastScroll >= siblingsTop) {
        self.$element.addClass(self.config.classFixed);
        self.$container.addClass(self.config.classFixedContainer);
      }
      else {
        self.$element.removeClass(self.config.classFixed);
        self.$container.removeClass(self.config.classFixedContainer);
      }
    }
  };

  /**
   * Theme implementation for mobile menu navigation element.
   *
   * @param {string} ul
   *   HTML list with navigation items to insert.
   *
   * @returns {string}
   *   The markup for mobile menu navigation.
   */
  Drupal.theme.prototype.victoryMobileMenuNav = function (ul) {
    return $('<nav id="responsive-nav" class="navbar-collapse"></nav>').append($(ul));
  };

  /**
   * Theme implementation for mobile menu parent link.
   *
   * @param {object} $link
   *   Link object to use for parent menu link rendering.
   *
   * @returns {string}
   *   The markup for the mobile menu parent link.
   */
  Drupal.theme.prototype.victoryMobileMenuParentLink = function ($link) {
    var $newLink = $('<a></a>').addClass($link.attr('class')).attr('href', $link.attr('href')).html($link.html());
    return $('<div class="' + Drupal.victory.mobileMenu.SELECTOR_LINK_PARENT_WRAPPER.substr(1) + '"></div>').append($newLink);
  };

  /**
   * Theme implementation for mobile menu back link.
   *
   * @param {object} $link
   *   Link object to use for back menu link rendering.
   *
   * @returns {string}
   *   The markup for the mobile menu back link.
   */
  Drupal.theme.prototype.victoryMobileMenuBackLink = function ($link) {
    var text = $link.length > 0 ? $link.html() : '';
    return $('<span class="' + Drupal.victory.mobileMenu.SELECTOR_LINK_BACK.substr(1) + '">' + text + '</span>');
  };

  /**
   * Theme implementation for mobile menu forward link.
   *
   * @returns {string}
   *   The markup for the mobile menu forward link.
   */
  Drupal.theme.prototype.victoryMobileMenuForwardLink = function () {
    return '<span class="' + Drupal.victory.mobileMenu.SELECTOR_LINK_FORWARD.substr(1) + '"></span>';
  };

  /**
   * Theme implementation for mobile menu link wrapper.
   *
   * @param {object} $link
   *   Link object to use for menu link wrapper rendering.
   *
   * @returns {string}
   *   The markup for the mobile menu link wrapper.
   */
  Drupal.theme.prototype.victoryMobileMenuLinkWrapper = function ($link) {
    return '<div class="' + Drupal.victory.mobileMenu.SELECTOR_LINK_WRAPPER.substr(1) + '"></div>';
  };

  /**
   * Theme implementation for the dropdown menu.
   *
   * @param {object} trigger
   *   Trigger DOM object for a menu dropdown.
   * @param {array} itemsList
   *   Array of HTML markup for menu links.
   *
   * @returns {string}
   *   The markup for the dropdown menu.
   */
  Drupal.theme.prototype.victoryMobileMenuDropdown = function (trigger, itemsList) {
    var containerClass = 'menu-nav-dropdown';
    var itemsClass = 'menu-nav-dropdown-items';
    var itemsId = 'menu-nav-tools-collapse';
    var $itemsList;
    var bootstrapComponent = 'collapse';
      // Without playing with the attributes the page jumps to the anchor.
    var $trigger = $(trigger).attr({
      'role': 'button',
      'data-toggle': bootstrapComponent,
      'data-target': '#' + itemsId,
      'aria-expanded': 'false',
      'aria-controls': itemsId
    }).removeAttr('href');

    $itemsList = $(itemsList).addClass(itemsClass);

    return '<div class="' + containerClass + '">' + $trigger.outerHTML() + '<div id="' + itemsId + '" class="spacing-wrapper' + ' ' + bootstrapComponent + '">' + $itemsList.outerHTML() + '</div></div>';
  };

  /**
   * Theme implementation for the off-canvas trigger button.
   *
   * @param {string} type
   *   Trigger type: open or close.
   * @param {string} title
   *   Optional trigger title.
   *
   * @returns {string}
   *   The markup for the off-canvas trigger button.
   */
  Drupal.theme.prototype.victoryOffcanvasTrigger = function (type, title) {
    title = title || '';
    return '<button type="button" class="js-offcanvas-trigger js-offcanvas-trigger-' + type + '" aria-controls="#responsive-nav" aria-expanded="false"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span><span class="title">' + title + '</span></button>';
  };

  /**
   * Get outer HTML of jQuery element.
   */
  $.fn.outerHTML = $.fn.outerHTML || function (s) {
    return s
      ? this.before(s).remove()
      : jQuery('<p>').append(this.eq(0).clone()).html();
  };

  // Animation frames polyfill. Use this instead of resize and scroll events as
  // browsers will paint in queues which increases performance and device
  // battery life.
  window.requestAnimFrame = (function () {
    return window.requestAnimationFrame ||
      window.webkitRequestAnimationFrame ||
      window.mozRequestAnimationFrame ||
      function (callback) {
        window.setTimeout(callback, 1000 / 60);
      };
  }());
}(jQuery, Drupal));
;
/* ========================================================================
 * Bootstrap: modal.js v3.3.7
 * http://getbootstrap.com/javascript/#modals
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // MODAL CLASS DEFINITION
  // ======================

  var Modal = function (element, options) {
    this.options             = options
    this.$body               = $(document.body)
    this.$element            = $(element)
    this.$dialog             = this.$element.find('.modal-dialog')
    this.$backdrop           = null
    this.isShown             = null
    this.originalBodyPad     = null
    this.scrollbarWidth      = 0
    this.ignoreBackdropClick = false

    if (this.options.remote) {
      this.$element
        .find('.modal-content')
        .load(this.options.remote, $.proxy(function () {
          this.$element.trigger('loaded.bs.modal')
        }, this))
    }
  }

  Modal.VERSION  = '3.3.7'

  Modal.TRANSITION_DURATION = 300
  Modal.BACKDROP_TRANSITION_DURATION = 150

  Modal.DEFAULTS = {
    backdrop: true,
    keyboard: true,
    show: true
  }

  Modal.prototype.toggle = function (_relatedTarget) {
    return this.isShown ? this.hide() : this.show(_relatedTarget)
  }

  Modal.prototype.show = function (_relatedTarget) {
    var that = this
    var e    = $.Event('show.bs.modal', { relatedTarget: _relatedTarget })

    this.$element.trigger(e)

    if (this.isShown || e.isDefaultPrevented()) return

    this.isShown = true

    this.checkScrollbar()
    this.setScrollbar()
    this.$body.addClass('modal-open')

    this.escape()
    this.resize()

    this.$element.on('click.dismiss.bs.modal', '[data-dismiss="modal"]', $.proxy(this.hide, this))

    this.$dialog.on('mousedown.dismiss.bs.modal', function () {
      that.$element.one('mouseup.dismiss.bs.modal', function (e) {
        if ($(e.target).is(that.$element)) that.ignoreBackdropClick = true
      })
    })

    this.backdrop(function () {
      var transition = $.support.transition && that.$element.hasClass('fade')

      if (!that.$element.parent().length) {
        that.$element.appendTo(that.$body) // don't move modals dom position
      }

      that.$element
        .show()
        .scrollTop(0)

      that.adjustDialog()

      if (transition) {
        that.$element[0].offsetWidth // force reflow
      }

      that.$element.addClass('in')

      that.enforceFocus()

      var e = $.Event('shown.bs.modal', { relatedTarget: _relatedTarget })

      transition ?
        that.$dialog // wait for modal to slide in
          .one('bsTransitionEnd', function () {
            that.$element.trigger('focus').trigger(e)
          })
          .emulateTransitionEnd(Modal.TRANSITION_DURATION) :
        that.$element.trigger('focus').trigger(e)
    })
  }

  Modal.prototype.hide = function (e) {
    if (e) e.preventDefault()

    e = $.Event('hide.bs.modal')

    this.$element.trigger(e)

    if (!this.isShown || e.isDefaultPrevented()) return

    this.isShown = false

    this.escape()
    this.resize()

    $(document).off('focusin.bs.modal')

    this.$element
      .removeClass('in')
      .off('click.dismiss.bs.modal')
      .off('mouseup.dismiss.bs.modal')

    this.$dialog.off('mousedown.dismiss.bs.modal')

    $.support.transition && this.$element.hasClass('fade') ?
      this.$element
        .one('bsTransitionEnd', $.proxy(this.hideModal, this))
        .emulateTransitionEnd(Modal.TRANSITION_DURATION) :
      this.hideModal()
  }

  Modal.prototype.enforceFocus = function () {
    $(document)
      .off('focusin.bs.modal') // guard against infinite focus loop
      .on('focusin.bs.modal', $.proxy(function (e) {
        if (document !== e.target &&
            this.$element[0] !== e.target &&
            !this.$element.has(e.target).length) {
          this.$element.trigger('focus')
        }
      }, this))
  }

  Modal.prototype.escape = function () {
    if (this.isShown && this.options.keyboard) {
      this.$element.on('keydown.dismiss.bs.modal', $.proxy(function (e) {
        e.which == 27 && this.hide()
      }, this))
    } else if (!this.isShown) {
      this.$element.off('keydown.dismiss.bs.modal')
    }
  }

  Modal.prototype.resize = function () {
    if (this.isShown) {
      $(window).on('resize.bs.modal', $.proxy(this.handleUpdate, this))
    } else {
      $(window).off('resize.bs.modal')
    }
  }

  Modal.prototype.hideModal = function () {
    var that = this
    this.$element.hide()
    this.backdrop(function () {
      that.$body.removeClass('modal-open')
      that.resetAdjustments()
      that.resetScrollbar()
      that.$element.trigger('hidden.bs.modal')
    })
  }

  Modal.prototype.removeBackdrop = function () {
    this.$backdrop && this.$backdrop.remove()
    this.$backdrop = null
  }

  Modal.prototype.backdrop = function (callback) {
    var that = this
    var animate = this.$element.hasClass('fade') ? 'fade' : ''

    if (this.isShown && this.options.backdrop) {
      var doAnimate = $.support.transition && animate

      this.$backdrop = $(document.createElement('div'))
        .addClass('modal-backdrop ' + animate)
        .appendTo(this.$body)

      this.$element.on('click.dismiss.bs.modal', $.proxy(function (e) {
        if (this.ignoreBackdropClick) {
          this.ignoreBackdropClick = false
          return
        }
        if (e.target !== e.currentTarget) return
        this.options.backdrop == 'static'
          ? this.$element[0].focus()
          : this.hide()
      }, this))

      if (doAnimate) this.$backdrop[0].offsetWidth // force reflow

      this.$backdrop.addClass('in')

      if (!callback) return

      doAnimate ?
        this.$backdrop
          .one('bsTransitionEnd', callback)
          .emulateTransitionEnd(Modal.BACKDROP_TRANSITION_DURATION) :
        callback()

    } else if (!this.isShown && this.$backdrop) {
      this.$backdrop.removeClass('in')

      var callbackRemove = function () {
        that.removeBackdrop()
        callback && callback()
      }
      $.support.transition && this.$element.hasClass('fade') ?
        this.$backdrop
          .one('bsTransitionEnd', callbackRemove)
          .emulateTransitionEnd(Modal.BACKDROP_TRANSITION_DURATION) :
        callbackRemove()

    } else if (callback) {
      callback()
    }
  }

  // these following methods are used to handle overflowing modals

  Modal.prototype.handleUpdate = function () {
    this.adjustDialog()
  }

  Modal.prototype.adjustDialog = function () {
    var modalIsOverflowing = this.$element[0].scrollHeight > document.documentElement.clientHeight

    this.$element.css({
      paddingLeft:  !this.bodyIsOverflowing && modalIsOverflowing ? this.scrollbarWidth : '',
      paddingRight: this.bodyIsOverflowing && !modalIsOverflowing ? this.scrollbarWidth : ''
    })
  }

  Modal.prototype.resetAdjustments = function () {
    this.$element.css({
      paddingLeft: '',
      paddingRight: ''
    })
  }

  Modal.prototype.checkScrollbar = function () {
    var fullWindowWidth = window.innerWidth
    if (!fullWindowWidth) { // workaround for missing window.innerWidth in IE8
      var documentElementRect = document.documentElement.getBoundingClientRect()
      fullWindowWidth = documentElementRect.right - Math.abs(documentElementRect.left)
    }
    this.bodyIsOverflowing = document.body.clientWidth < fullWindowWidth
    this.scrollbarWidth = this.measureScrollbar()
  }

  Modal.prototype.setScrollbar = function () {
    var bodyPad = parseInt((this.$body.css('padding-right') || 0), 10)
    this.originalBodyPad = document.body.style.paddingRight || ''
    if (this.bodyIsOverflowing) this.$body.css('padding-right', bodyPad + this.scrollbarWidth)
  }

  Modal.prototype.resetScrollbar = function () {
    this.$body.css('padding-right', this.originalBodyPad)
  }

  Modal.prototype.measureScrollbar = function () { // thx walsh
    var scrollDiv = document.createElement('div')
    scrollDiv.className = 'modal-scrollbar-measure'
    this.$body.append(scrollDiv)
    var scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth
    this.$body[0].removeChild(scrollDiv)
    return scrollbarWidth
  }


  // MODAL PLUGIN DEFINITION
  // =======================

  function Plugin(option, _relatedTarget) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.modal')
      var options = $.extend({}, Modal.DEFAULTS, $this.data(), typeof option == 'object' && option)

      if (!data) $this.data('bs.modal', (data = new Modal(this, options)))
      if (typeof option == 'string') data[option](_relatedTarget)
      else if (options.show) data.show(_relatedTarget)
    })
  }

  var old = $.fn.modal

  $.fn.modal             = Plugin
  $.fn.modal.Constructor = Modal


  // MODAL NO CONFLICT
  // =================

  $.fn.modal.noConflict = function () {
    $.fn.modal = old
    return this
  }


  // MODAL DATA-API
  // ==============

  $(document).on('click.bs.modal.data-api', '[data-toggle="modal"]', function (e) {
    var $this   = $(this)
    var href    = $this.attr('href')
    var $target = $($this.attr('data-target') || (href && href.replace(/.*(?=#[^\s]+$)/, ''))) // strip for ie7
    var option  = $target.data('bs.modal') ? 'toggle' : $.extend({ remote: !/#/.test(href) && href }, $target.data(), $this.data())

    if ($this.is('a')) e.preventDefault()

    $target.one('show.bs.modal', function (showEvent) {
      if (showEvent.isDefaultPrevented()) return // only register focus restorer if modal will actually get shown
      $target.one('hidden.bs.modal', function () {
        $this.is(':visible') && $this.trigger('focus')
      })
    })
    Plugin.call($target, option, this)
  })

}(jQuery);
;
/* ========================================================================
 * Bootstrap: collapse.js v3.3.7
 * http://getbootstrap.com/javascript/#collapse
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */

/* jshint latedef: false */

+function ($) {
  'use strict';

  // COLLAPSE PUBLIC CLASS DEFINITION
  // ================================

  var Collapse = function (element, options) {
    this.$element      = $(element)
    this.options       = $.extend({}, Collapse.DEFAULTS, options)
    this.$trigger      = $('[data-toggle="collapse"][href="#' + element.id + '"],' +
                           '[data-toggle="collapse"][data-target="#' + element.id + '"]')
    this.transitioning = null

    if (this.options.parent) {
      this.$parent = this.getParent()
    } else {
      this.addAriaAndCollapsedClass(this.$element, this.$trigger)
    }

    if (this.options.toggle) this.toggle()
  }

  Collapse.VERSION  = '3.3.7'

  Collapse.TRANSITION_DURATION = 350

  Collapse.DEFAULTS = {
    toggle: true
  }

  Collapse.prototype.dimension = function () {
    var hasWidth = this.$element.hasClass('width')
    return hasWidth ? 'width' : 'height'
  }

  Collapse.prototype.show = function () {
    if (this.transitioning || this.$element.hasClass('in')) return

    var activesData
    var actives = this.$parent && this.$parent.children('.panel').children('.in, .collapsing')

    if (actives && actives.length) {
      activesData = actives.data('bs.collapse')
      if (activesData && activesData.transitioning) return
    }

    var startEvent = $.Event('show.bs.collapse')
    this.$element.trigger(startEvent)
    if (startEvent.isDefaultPrevented()) return

    if (actives && actives.length) {
      Plugin.call(actives, 'hide')
      activesData || actives.data('bs.collapse', null)
    }

    var dimension = this.dimension()

    this.$element
      .removeClass('collapse')
      .addClass('collapsing')[dimension](0)
      .attr('aria-expanded', true)

    this.$trigger
      .removeClass('collapsed')
      .attr('aria-expanded', true)

    this.transitioning = 1

    var complete = function () {
      this.$element
        .removeClass('collapsing')
        .addClass('collapse in')[dimension]('')
      this.transitioning = 0
      this.$element
        .trigger('shown.bs.collapse')
    }

    if (!$.support.transition) return complete.call(this)

    var scrollSize = $.camelCase(['scroll', dimension].join('-'))

    this.$element
      .one('bsTransitionEnd', $.proxy(complete, this))
      .emulateTransitionEnd(Collapse.TRANSITION_DURATION)[dimension](this.$element[0][scrollSize])
  }

  Collapse.prototype.hide = function () {
    if (this.transitioning || !this.$element.hasClass('in')) return

    var startEvent = $.Event('hide.bs.collapse')
    this.$element.trigger(startEvent)
    if (startEvent.isDefaultPrevented()) return

    var dimension = this.dimension()

    this.$element[dimension](this.$element[dimension]())[0].offsetHeight

    this.$element
      .addClass('collapsing')
      .removeClass('collapse in')
      .attr('aria-expanded', false)

    this.$trigger
      .addClass('collapsed')
      .attr('aria-expanded', false)

    this.transitioning = 1

    var complete = function () {
      this.transitioning = 0
      this.$element
        .removeClass('collapsing')
        .addClass('collapse')
        .trigger('hidden.bs.collapse')
    }

    if (!$.support.transition) return complete.call(this)

    this.$element
      [dimension](0)
      .one('bsTransitionEnd', $.proxy(complete, this))
      .emulateTransitionEnd(Collapse.TRANSITION_DURATION)
  }

  Collapse.prototype.toggle = function () {
    this[this.$element.hasClass('in') ? 'hide' : 'show']()
  }

  Collapse.prototype.getParent = function () {
    return $(this.options.parent)
      .find('[data-toggle="collapse"][data-parent="' + this.options.parent + '"]')
      .each($.proxy(function (i, element) {
        var $element = $(element)
        this.addAriaAndCollapsedClass(getTargetFromTrigger($element), $element)
      }, this))
      .end()
  }

  Collapse.prototype.addAriaAndCollapsedClass = function ($element, $trigger) {
    var isOpen = $element.hasClass('in')

    $element.attr('aria-expanded', isOpen)
    $trigger
      .toggleClass('collapsed', !isOpen)
      .attr('aria-expanded', isOpen)
  }

  function getTargetFromTrigger($trigger) {
    var href
    var target = $trigger.attr('data-target')
      || (href = $trigger.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '') // strip for ie7

    return $(target)
  }


  // COLLAPSE PLUGIN DEFINITION
  // ==========================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.collapse')
      var options = $.extend({}, Collapse.DEFAULTS, $this.data(), typeof option == 'object' && option)

      if (!data && options.toggle && /show|hide/.test(option)) options.toggle = false
      if (!data) $this.data('bs.collapse', (data = new Collapse(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.collapse

  $.fn.collapse             = Plugin
  $.fn.collapse.Constructor = Collapse


  // COLLAPSE NO CONFLICT
  // ====================

  $.fn.collapse.noConflict = function () {
    $.fn.collapse = old
    return this
  }


  // COLLAPSE DATA-API
  // =================

  $(document).on('click.bs.collapse.data-api', '[data-toggle="collapse"]', function (e) {
    var $this   = $(this)

    if (!$this.attr('data-target')) e.preventDefault()

    var $target = getTargetFromTrigger($this)
    var data    = $target.data('bs.collapse')
    var option  = data ? 'toggle' : $this.data()

    Plugin.call($target, option)
  })

}(jQuery);
;
/**
 * @file
 * Behaviours for Shutter widget.
 */

/* global jQuery, Drupal */

(function ($, Drupal) {
  'use strict';

  Drupal.behaviors.victoryShutter = {
    attach: function (context, settings) {
      // Do not attach to AJAX responses.
      if (context !== window.document) {
        return;
      }
      var self = this;

      // Init shutter widget.
      var $shutter = self.initShutter($('.js-shutter', context));

      $shutter
      .on('shown.shutter', function () {
        self.resetSearch();

        if (self.isIDevice()) {
          $('body').data('lastScroll', $(window).scrollTop());
          window.scrollTo(0, 0);
        }
      })
      .on('hidden.shutter', function () {
        if (self.isIDevice()) {
          var lastScroll = $('body').data('lastScroll');
          if (lastScroll) {
            setTimeout(function () {
              window.scrollTo(0, lastScroll);
            }, 100);
          }
        }
      });

      // Bind custom shutter close button's click.
      $('.js-shutter-close', $shutter).on('click', function () {
        var $shutter = $(this).parents('.js-shutter');
        $shutter.modal('hide');
      });

      // Workaround for Marketo timing bug.
      //
      // If Marketo/munchkin is configured to track a link that is a modal trigger,
      // intermittently the default action (open the href) will be triggered.
      //
      // The workaround here is to change the href so Marketo doesn't try to track
      // the click.
      var $loginLink = $('[data-shutter-item-target="#block-menu-block-main-menu-tools"]');
      $loginLink.attr('href', '#' + $loginLink.attr('href'));

      // Clone search button to mobile menu bar.
      $('[data-shutter-item-target="#block-google-appliance-ga-block-search-form"]', context).clone().insertAfter('.js-responsive-menu-trigger-anchor');
    },

    /**
     * Init shutter widget.
     */
    initShutter: function ($shutter) {
      var self = this;

      // Add default classes to all items.
      $shutter.find('.js-shutter-items').children(':not(.js-shutter-item)').addClass('js-shutter-item');

      $shutter.on('show.bs.modal.victoryShutter', function (event) {
        // Show items for this trigger when modal is about to be fully opened.
        self.shutterShowItems($(this), $(event.relatedTarget));
      }).on('shown.bs.modal.victoryShutter', function () {
        // Expand shutter when modal is fully opened.
        self.shutterExpand($(this));
      }).on('hide.bs.modal.victoryShutter', function (event) {
        // Close shutter when modal is closed.
        self.shutterCollapse($(this), event);
      });

      return $shutter;
    },

    /**
     * Expand specified shutter.
     */
    shutterExpand: function ($shutter) {
      var $dialog = $shutter.find('.js-shutter-dialog');
      $dialog.collapse('show');
      var e = $.Event('shown.shutter');
      $shutter.trigger(e);
    },

    /**
     * Collapse specified shutter.
     */
    shutterCollapse: function ($shutter, event) {
      var $dialog = $shutter.find('.js-shutter-dialog');

      if ($dialog.hasClass('in')) {
        event.preventDefault();
        $dialog.collapse('hide');
        setTimeout(function () {
          $shutter.modal('hide');
        }, jQuery.fn.collapse.prototype.constructor.Constructor.TRANSITION_DURATION);
      }
      else {
        var e = $.Event('hidden.shutter');
        $shutter.trigger(e);
      }
    },

    /**
     * Show items for specified shutter.
     */
    shutterShowItems: function ($shutter, $trigger) {
      var $recipient = $($trigger.data('shutter-item-target'));
      $shutter.find('.js-shutter-item').not($recipient).removeClass('js-shutter-item-shown');
      $recipient.addClass('js-shutter-item-shown');
    },

    /**
     * Search block customisations.
     */
    resetSearch: function ($target) {
      var $search = $('#block-google-appliance-ga-block-search-form').find('input[name="search_keys"]');
      $search.val('');
      $search.focus();
    },

    /**
     * Check that current device is iPhone or iPad.
     */
    isIDevice: function () {
      return navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/iPad/i);
    }
  };
}(jQuery, Drupal));
;
/* ========================================================================
 * Bootstrap: affix.js v3.3.7
 * http://getbootstrap.com/javascript/#affix
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // AFFIX CLASS DEFINITION
  // ======================

  var Affix = function (element, options) {
    this.options = $.extend({}, Affix.DEFAULTS, options)

    this.$target = $(this.options.target)
      .on('scroll.bs.affix.data-api', $.proxy(this.checkPosition, this))
      .on('click.bs.affix.data-api',  $.proxy(this.checkPositionWithEventLoop, this))

    this.$element     = $(element)
    this.affixed      = null
    this.unpin        = null
    this.pinnedOffset = null

    this.checkPosition()
  }

  Affix.VERSION  = '3.3.7'

  Affix.RESET    = 'affix affix-top affix-bottom'

  Affix.DEFAULTS = {
    offset: 0,
    target: window
  }

  Affix.prototype.getState = function (scrollHeight, height, offsetTop, offsetBottom) {
    var scrollTop    = this.$target.scrollTop()
    var position     = this.$element.offset()
    var targetHeight = this.$target.height()

    if (offsetTop != null && this.affixed == 'top') return scrollTop < offsetTop ? 'top' : false

    if (this.affixed == 'bottom') {
      if (offsetTop != null) return (scrollTop + this.unpin <= position.top) ? false : 'bottom'
      return (scrollTop + targetHeight <= scrollHeight - offsetBottom) ? false : 'bottom'
    }

    var initializing   = this.affixed == null
    var colliderTop    = initializing ? scrollTop : position.top
    var colliderHeight = initializing ? targetHeight : height

    if (offsetTop != null && scrollTop <= offsetTop) return 'top'
    if (offsetBottom != null && (colliderTop + colliderHeight >= scrollHeight - offsetBottom)) return 'bottom'

    return false
  }

  Affix.prototype.getPinnedOffset = function () {
    if (this.pinnedOffset) return this.pinnedOffset
    this.$element.removeClass(Affix.RESET).addClass('affix')
    var scrollTop = this.$target.scrollTop()
    var position  = this.$element.offset()
    return (this.pinnedOffset = position.top - scrollTop)
  }

  Affix.prototype.checkPositionWithEventLoop = function () {
    setTimeout($.proxy(this.checkPosition, this), 1)
  }

  Affix.prototype.checkPosition = function () {
    if (!this.$element.is(':visible')) return

    var height       = this.$element.height()
    var offset       = this.options.offset
    var offsetTop    = offset.top
    var offsetBottom = offset.bottom
    var scrollHeight = Math.max($(document).height(), $(document.body).height())

    if (typeof offset != 'object')         offsetBottom = offsetTop = offset
    if (typeof offsetTop == 'function')    offsetTop    = offset.top(this.$element)
    if (typeof offsetBottom == 'function') offsetBottom = offset.bottom(this.$element)

    var affix = this.getState(scrollHeight, height, offsetTop, offsetBottom)

    if (this.affixed != affix) {
      if (this.unpin != null) this.$element.css('top', '')

      var affixType = 'affix' + (affix ? '-' + affix : '')
      var e         = $.Event(affixType + '.bs.affix')

      this.$element.trigger(e)

      if (e.isDefaultPrevented()) return

      this.affixed = affix
      this.unpin = affix == 'bottom' ? this.getPinnedOffset() : null

      this.$element
        .removeClass(Affix.RESET)
        .addClass(affixType)
        .trigger(affixType.replace('affix', 'affixed') + '.bs.affix')
    }

    if (affix == 'bottom') {
      this.$element.offset({
        top: scrollHeight - height - offsetBottom
      })
    }
  }


  // AFFIX PLUGIN DEFINITION
  // =======================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.affix')
      var options = typeof option == 'object' && option

      if (!data) $this.data('bs.affix', (data = new Affix(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.affix

  $.fn.affix             = Plugin
  $.fn.affix.Constructor = Affix


  // AFFIX NO CONFLICT
  // =================

  $.fn.affix.noConflict = function () {
    $.fn.affix = old
    return this
  }


  // AFFIX DATA-API
  // ==============

  $(window).on('load', function () {
    $('[data-spy="affix"]').each(function () {
      var $spy = $(this)
      var data = $spy.data()

      data.offset = data.offset || {}

      if (data.offsetBottom != null) data.offset.bottom = data.offsetBottom
      if (data.offsetTop    != null) data.offset.top    = data.offsetTop

      Plugin.call($spy, data)
    })
  })

}(jQuery);
;
/* ========================================================================
 * Bootstrap: scrollspy.js v3.3.7
 * http://getbootstrap.com/javascript/#scrollspy
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // SCROLLSPY CLASS DEFINITION
  // ==========================

  function ScrollSpy(element, options) {
    this.$body          = $(document.body)
    this.$scrollElement = $(element).is(document.body) ? $(window) : $(element)
    this.options        = $.extend({}, ScrollSpy.DEFAULTS, options)
    this.selector       = (this.options.target || '') + ' .nav li > a'
    this.offsets        = []
    this.targets        = []
    this.activeTarget   = null
    this.scrollHeight   = 0

    this.$scrollElement.on('scroll.bs.scrollspy', $.proxy(this.process, this))
    this.refresh()
    this.process()
  }

  ScrollSpy.VERSION  = '3.3.7'

  ScrollSpy.DEFAULTS = {
    offset: 10
  }

  ScrollSpy.prototype.getScrollHeight = function () {
    return this.$scrollElement[0].scrollHeight || Math.max(this.$body[0].scrollHeight, document.documentElement.scrollHeight)
  }

  ScrollSpy.prototype.refresh = function () {
    var that          = this
    var offsetMethod  = 'offset'
    var offsetBase    = 0

    this.offsets      = []
    this.targets      = []
    this.scrollHeight = this.getScrollHeight()

    if (!$.isWindow(this.$scrollElement[0])) {
      offsetMethod = 'position'
      offsetBase   = this.$scrollElement.scrollTop()
    }

    this.$body
      .find(this.selector)
      .map(function () {
        var $el   = $(this)
        var href  = $el.data('target') || $el.attr('href')
        var $href = /^#./.test(href) && $(href)

        return ($href
          && $href.length
          && $href.is(':visible')
          && [[$href[offsetMethod]().top + offsetBase, href]]) || null
      })
      .sort(function (a, b) { return a[0] - b[0] })
      .each(function () {
        that.offsets.push(this[0])
        that.targets.push(this[1])
      })
  }

  ScrollSpy.prototype.process = function () {
    var scrollTop    = this.$scrollElement.scrollTop() + this.options.offset
    var scrollHeight = this.getScrollHeight()
    var maxScroll    = this.options.offset + scrollHeight - this.$scrollElement.height()
    var offsets      = this.offsets
    var targets      = this.targets
    var activeTarget = this.activeTarget
    var i

    if (this.scrollHeight != scrollHeight) {
      this.refresh()
    }

    if (scrollTop >= maxScroll) {
      return activeTarget != (i = targets[targets.length - 1]) && this.activate(i)
    }

    if (activeTarget && scrollTop < offsets[0]) {
      this.activeTarget = null
      return this.clear()
    }

    for (i = offsets.length; i--;) {
      activeTarget != targets[i]
        && scrollTop >= offsets[i]
        && (offsets[i + 1] === undefined || scrollTop < offsets[i + 1])
        && this.activate(targets[i])
    }
  }

  ScrollSpy.prototype.activate = function (target) {
    this.activeTarget = target

    this.clear()

    var selector = this.selector +
      '[data-target="' + target + '"],' +
      this.selector + '[href="' + target + '"]'

    var active = $(selector)
      .parents('li')
      .addClass('active')

    if (active.parent('.dropdown-menu').length) {
      active = active
        .closest('li.dropdown')
        .addClass('active')
    }

    active.trigger('activate.bs.scrollspy')
  }

  ScrollSpy.prototype.clear = function () {
    $(this.selector)
      .parentsUntil(this.options.target, '.active')
      .removeClass('active')
  }


  // SCROLLSPY PLUGIN DEFINITION
  // ===========================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.scrollspy')
      var options = typeof option == 'object' && option

      if (!data) $this.data('bs.scrollspy', (data = new ScrollSpy(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.scrollspy

  $.fn.scrollspy             = Plugin
  $.fn.scrollspy.Constructor = ScrollSpy


  // SCROLLSPY NO CONFLICT
  // =====================

  $.fn.scrollspy.noConflict = function () {
    $.fn.scrollspy = old
    return this
  }


  // SCROLLSPY DATA-API
  // ==================

  $(window).on('load.bs.scrollspy.data-api', function () {
    $('[data-spy="scroll"]').each(function () {
      var $spy = $(this)
      Plugin.call($spy, $spy.data())
    })
  })

}(jQuery);
;
/**
 * @file
 * Behaviours for Sticky header widget.
 */

/* global jQuery, Drupal */

(function ($, Drupal) {
  'use strict';

  Drupal.behaviors.victoryStickyHeader = {
    attach: function (context, settings) {
      // Don't attempt to attach if AJAX response or header present.
      if (context !== window.document || $('.sticky-header').length) {
        return;
      }
      var offset = $('header[role="banner"]', context).offset().top + $('header[role="banner"]', context).outerHeight();
      var $header = $(Drupal.theme('victoryStickyHeader')).appendTo('body');
      $header.attr('data-spy', 'affix');
      $header.attr('data-offset-top', offset);

      var $logo = $('#logo').clone().addClass('diamond-only');
      $logo.appendTo($header.find('.logo-container'));

      // Use node title or fallback to html <title> tag value.
      var pageTitle = $('.node-title', context).first().text().trim();
      if (pageTitle === '') {
        pageTitle = $('title', context).first().text();
        pageTitle = pageTitle.substring(0, pageTitle.indexOf('|')).trim();
      }
      $header.find('.page-title').text(pageTitle);

      var $searchTrigger = $('[data-shutter-item-target="#block-google-appliance-ga-block-search-form"]', context).first().clone();
      $searchTrigger.appendTo($header.find('.search-container'));
    }
  };

  /**
   * Theme implementation for sticky header.
   *
   * @returns {string}
   *   Header markup.
   */
  Drupal.theme.prototype.victoryStickyHeader = function () {
    var markup =
      '<div class="sticky-header"><div class="container"><div class="col-md-1 logo-container"></div><div class="col-md-10 page-title"></div><div class="col-md-1 search-container"></div></div></div>';

    return markup;
  };
}(jQuery, Drupal));
;
/* ========================================================================
 * Bootstrap: tab.js v3.3.7
 * http://getbootstrap.com/javascript/#tabs
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // TAB CLASS DEFINITION
  // ====================

  var Tab = function (element) {
    // jscs:disable requireDollarBeforejQueryAssignment
    this.element = $(element)
    // jscs:enable requireDollarBeforejQueryAssignment
  }

  Tab.VERSION = '3.3.7'

  Tab.TRANSITION_DURATION = 150

  Tab.prototype.show = function () {
    var $this    = this.element
    var $ul      = $this.closest('ul:not(.dropdown-menu)')
    var selector = $this.data('target')

    if (!selector) {
      selector = $this.attr('href')
      selector = selector && selector.replace(/.*(?=#[^\s]*$)/, '') // strip for ie7
    }

    if ($this.parent('li').hasClass('active')) return

    var $previous = $ul.find('.active:last a')
    var hideEvent = $.Event('hide.bs.tab', {
      relatedTarget: $this[0]
    })
    var showEvent = $.Event('show.bs.tab', {
      relatedTarget: $previous[0]
    })

    $previous.trigger(hideEvent)
    $this.trigger(showEvent)

    if (showEvent.isDefaultPrevented() || hideEvent.isDefaultPrevented()) return

    var $target = $(selector)

    this.activate($this.closest('li'), $ul)
    this.activate($target, $target.parent(), function () {
      $previous.trigger({
        type: 'hidden.bs.tab',
        relatedTarget: $this[0]
      })
      $this.trigger({
        type: 'shown.bs.tab',
        relatedTarget: $previous[0]
      })
    })
  }

  Tab.prototype.activate = function (element, container, callback) {
    var $active    = container.find('> .active')
    var transition = callback
      && $.support.transition
      && ($active.length && $active.hasClass('fade') || !!container.find('> .fade').length)

    function next() {
      $active
        .removeClass('active')
        .find('> .dropdown-menu > .active')
          .removeClass('active')
        .end()
        .find('[data-toggle="tab"]')
          .attr('aria-expanded', false)

      element
        .addClass('active')
        .find('[data-toggle="tab"]')
          .attr('aria-expanded', true)

      if (transition) {
        element[0].offsetWidth // reflow for transition
        element.addClass('in')
      } else {
        element.removeClass('fade')
      }

      if (element.parent('.dropdown-menu').length) {
        element
          .closest('li.dropdown')
            .addClass('active')
          .end()
          .find('[data-toggle="tab"]')
            .attr('aria-expanded', true)
      }

      callback && callback()
    }

    $active.length && transition ?
      $active
        .one('bsTransitionEnd', next)
        .emulateTransitionEnd(Tab.TRANSITION_DURATION) :
      next()

    $active.removeClass('in')
  }


  // TAB PLUGIN DEFINITION
  // =====================

  function Plugin(option) {
    return this.each(function () {
      var $this = $(this)
      var data  = $this.data('bs.tab')

      if (!data) $this.data('bs.tab', (data = new Tab(this)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.tab

  $.fn.tab             = Plugin
  $.fn.tab.Constructor = Tab


  // TAB NO CONFLICT
  // ===============

  $.fn.tab.noConflict = function () {
    $.fn.tab = old
    return this
  }


  // TAB DATA-API
  // ============

  var clickHandler = function (e) {
    e.preventDefault()
    Plugin.call($(this), 'show')
  }

  $(document)
    .on('click.bs.tab.data-api', '[data-toggle="tab"]', clickHandler)
    .on('click.bs.tab.data-api', '[data-toggle="pill"]', clickHandler)

}(jQuery);
;
/**
 * @file
 * On this page nav JS.
 */

/* global jQuery, Drupal */

(function ($, Drupal) {
  'use strict';

  // Migrated legacy VU theme slug function.
  function convert_title_to_anchor(h2_text) {
    var gotoText = h2_text.replace(/\s+/g, '-').replace(/[^A-Za-z0-9\-]/, '').replace(/-+/, '-').toLowerCase();
    return 'goto-' + gotoText;
  }

  // Migrated legacy VU theme: convert headings to anchor list.
  function generate_list_for_anchor(headings) {
    var items = '';
    $(headings).each(function () {
      var heading_id = typeof ($(this).attr('id')) !== 'undefined' ? $(this).attr('id') : convert_title_to_anchor($(this).text());
      if (!$(this).hasClass('element-invisible') && !$(this).hasClass('exclude-onthispage') && $(this).is(':visible')) {
        items += '<li><a href="#' + heading_id + '" data-smoothscroll>' + $(this).text() + '</a></li>';
      }
    });
    if (items.length > 0) {
      return '<ul>' + items + '</ul>';
    }
  }

  var self = Drupal.behaviors.victoryOnThisPage = {
    BLOCK_ID: '#block-vu-core-vu-on-page-nav',
    CONTENT_ID: '#block-vu-core-vu-on-page-nav__content',
    MAX_HEIGHT: 315,

    menu_items: null,
    mobile: window.matchMedia('(max-width: 767px)'),

    attach: function (context) {
      // Do not attach to AJAX responses.
      if (context !== window.document) {
        return;
      }
      // Populate legacy JS based menu.
      $('.node-type-courses ' + self.CONTENT_ID + ', .page-courses-international ' + self.CONTENT_ID).html(function () {
        var selector = '.node-type-courses div.main-content h2, .page-courses-international div.main-content h2';
        $(selector).each(function () {
          if (typeof $(this).attr('id') === 'undefined') {
            $(this).attr('id', convert_title_to_anchor($(this).text()));
          }
        });

        // Exclude sidebar h2. This currently has to target .col-md-4.
        $('.node-type-courses div.main-content .col-md-4 h2, .page-courses-international div.main-content .col-md-4 h2').addClass('exclude-onthispage');

        return generate_list_for_anchor(selector);
      });

      // Save initial menu state.
      if (self.menu_items === null) {
        self.menu_items = $(self.CONTENT_ID).html();
      }

      // Attach window resize handling.
      $(window).resize(function () {
        self.reflow();
      });
      self.reflow();
    },

    reflow: function () {
      // Restore default menu.
      $(self.CONTENT_ID).html(self.menu_items);

      // Re-flow the menu if necessary.
      if ($(self.BLOCK_ID).height() > self.MAX_HEIGHT) {
        // Build the extended menu using Bootstrap Collapse.
        var $more_menu = $('<ul id="block-vu-core-vu-on-page-nav__more" class="collapse">');
        $('<li><a class="more" data-toggle="collapse" href="#block-vu-core-vu-on-page-nav__more" aria-expanded="false" aria-controls="block-vu-core-vu-on-page-nav__more">More</a></li>').appendTo($(self.CONTENT_ID).find('ul'));

        // Move items from main menu to extended menu until main menu is less than maximum height.
        while ($(self.BLOCK_ID).height() > self.MAX_HEIGHT) {
          $more_menu.prepend($(self.CONTENT_ID).find('li')[$(self.CONTENT_ID).find('li').length - 2]);
        }

        // Attach tab navigation behaviour.
        $('li', $more_menu).last().bind('keydown', function (e) {
          if (e.keyCode === 9 && e.shiftKey === false) {
            $('#block-vu-core-vu-on-page-nav__more').collapse('hide');
          }
        });

        // Attach extended menu.
        $more_menu.appendTo(self.CONTENT_ID);
      }
    }
  };
})(jQuery, Drupal);
;
/**
 * @file
 * Javascript behaviours for the Accordion paragraph type.
 */

/* global jQuery, Drupal */

(function ($) {
  'use strict';
  Drupal.behaviors.vuCoreAccordion = {
    attach: function (context, settings) {
      $(document).on('hide.bs.collapse', '.accordion-inner', function (e) {
        $(this).removeClass('accordion-inner--active');
      }).on('show.bs.collapse', '.accordion-inner', function (e) {
        $(this).addClass('accordion-inner--active');
      });
    }
  };
})(jQuery);
;
