/**
 * @file
 * Client-side formatting of chat opening times.
 *
 * This is required to support cached pages.
 */

/* global jQuery, Drupal, moment */

(function ($, Drupal) {
  'use strict';

  Drupal.behaviors.vuChatFooter = {
    attach: function (context, settings) {
      if (!moment || typeof settings['vu_chat_footer'] === 'undefined') {
        return;
      }

      this.initTimezone();

      var times = settings.vu_chat_footer.times || {};
      times = this.sortTimes(times);
      var state = this.getState(times);

      var title = state.isOpen ? settings.vu_chat_footer.open_title : settings.vu_chat_footer.close_title;
      var message = state.isOpen ? settings.vu_chat_footer.open_message : settings.vu_chat_footer.close_message;
      var hoursTitle = state.isOpen ? settings.vu_chat_footer.hours_open_title : settings.vu_chat_footer.hours_close_title;
      var timezone = settings.vu_chat_footer.hours_timezone;

      $('.js-chat-status-title', context).text(title);
      $('.js-chat-status-message', context).text(message);

      if (state.start && state.finish) {
        $('.js-chat-hours-title', context).text(hoursTitle);
        $('.js-chat-hours', context).html(this.renderHours(state.weekday, state.start, state.finish, timezone));
      }
    },

    /**
     * Render hours string.
     */
    renderHours: function (weekday, start, finish, timezone) {
      return weekday.substring(0, 3) + ', ' + start + ' &ndash; ' + finish + ' (' + timezone + ')';
    },

    /**
     * Init current server timezone.
     */
    initTimezone: function () {
      // Add Australia/Melbourne to the zones.
      if (!moment.tz.zone('Australia/Melbourne')) {
        moment.tz.add('Australia/Melbourne|AEST AEDT|-a0 -b0|0101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101|-293lX xcX 10jd0 yL0 1cN0 1cL0 1fB0 19X0 17c10 LA0 1C00 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 Rc0 1zc0 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 Rc0 1zc0 Oo0 1zc0 Oo0 1zc0 Oo0 1zc0 U00 1qM0 WM0 1qM0 11A0 1tA0 U00 1tA0 U00 1tA0 Oo0 1zc0 Oo0 1zc0 Rc0 1zc0 Oo0 1zc0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 11A0 1o00 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 11A0 1o00 WM0 1qM0 14o0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0|39e5');
      }
    },

    /**
     * Get Australian date for today.
     */
    getTodayDate: function () {
      return moment().tz('Australia/Melbourne');
    },

    /**
     * Get today day of the week.
     */
    getTodayWeekday: function (now) {
      return now ? now.format('dddd') : this.getTodayDate().format('dddd');
    },

    /**
     * Get next week day.
     */
    getNextWeekday: function (times, now) {
      now = now || this.getTodayDate();
      now = now.clone();
      for (var i = 1; i < 8; i++) {
        var nextDay = now.add(1, 'days').format('dddd');
        if (this.getTimesForWeekday(times, nextDay)) {
          return nextDay;
        }
      }
      return false;
    },

    /**
     * Get hours for current day.
     */
    getTodayHours: function (times, now) {
      now = now || this.getTodayDate();
      var weekday = this.getTodayWeekday(now);

      return this.getTimesForWeekday(times, weekday);
    },

    /**
     * Get hours for next weekday.
     */
    getNextWeekdayHours: function (times, now) {
      var weekday = this.getNextWeekday(times, now);
      return this.getTimesForWeekday(times, weekday);
    },

    /**
     * Get chat state.
     */
    getState: function (times, now) {
      var state = {
        isOpen: null,
        start: null,
        finish: null,
        weekday: null
      };

      now = now || this.getTodayDate();
      var todayHours = this.getTodayHours(times, now);
      if (!todayHours) {
        state.weekday = this.getNextWeekday(times, now);
        hours = this.getNextWeekdayHours(times, now);
        state.isOpen = false;
        state.start = 'start_time' in hours ? hours['start_time'] : null;
        state.finish = 'end_time' in hours ? hours['end_time'] : null;
        return state;
      }

      var start = moment.tz(now.format('DD-MM-YY') + ' ' + todayHours['start_time'], ['DD-MM-YY h:m a'], 'Australia/Melbourne');
      var finish = moment.tz(now.format('DD-MM-YY') + ' ' + todayHours['end_time'], ['DD-MM-YY h:m a'], 'Australia/Melbourne');

      if (!start || !finish) {
        return false;
      }

      if (start > finish) {
        return false;
      }

      var hours;
      if (now.isBefore(start)) {
        hours = todayHours;
        state.isOpen = false;
        state.weekday = this.getTodayWeekday(now);
      }
      else if (now.isAfter(finish)) {
        hours = this.getNextWeekdayHours(times, now);
        state.isOpen = false;
        state.weekday = this.getNextWeekday(times, now);
      }
      else {
        hours = todayHours;
        state.isOpen = true;
        state.weekday = this.getTodayWeekday(now);
      }
      state.start = 'start_time' in hours ? hours['start_time'] : null;
      state.finish = 'end_time' in hours ? hours['end_time'] : null;

      return state;
    },

    /**
     * Sort times according to the order of the days of the week.
     */
    sortTimes: function (times) {
      var newTimes = [];
      var weekdays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
      for (var idx in weekdays) {
        if (weekdays[idx] in times) {
          newTimes.push($.extend({}, times[weekdays[idx]], {weekday: weekdays[idx]}));
        }
      }
      return newTimes;
    },

    /**
     * Get times for specified day.
     */
    getTimesForWeekday: function (times, weekday) {
      var dayTimes = null;
      for (var idx in times) {
        if (times[idx].weekday === weekday) {
          dayTimes = times[idx];
          break;
        }
      }
      return dayTimes;
    }
  };
}(jQuery, Drupal));
;
(function ($) {

Drupal.googleanalytics = {};

$(document).ready(function() {

  // Attach mousedown, keyup, touchstart events to document only and catch
  // clicks on all elements.
  $(document.body).bind("mousedown keyup touchstart", function(event) {

    // Catch the closest surrounding link of a clicked element.
    $(event.target).closest("a,area").each(function() {

      // Is the clicked URL internal?
      if (Drupal.googleanalytics.isInternal(this.href)) {
        // Skip 'click' tracking, if custom tracking events are bound.
        if ($(this).is('.colorbox') && (Drupal.settings.googleanalytics.trackColorbox)) {
          // Do nothing here. The custom event will handle all tracking.
          //console.info("Click on .colorbox item has been detected.");
        }
        // Is download tracking activated and the file extension configured for download tracking?
        else if (Drupal.settings.googleanalytics.trackDownload && Drupal.googleanalytics.isDownload(this.href)) {
          // Download link clicked.
          ga("send", {
            "hitType": "event",
            "eventCategory": "Downloads",
            "eventAction": Drupal.googleanalytics.getDownloadExtension(this.href).toUpperCase(),
            "eventLabel": Drupal.googleanalytics.getPageUrl(this.href),
            "transport": "beacon"
          });
        }
        else if (Drupal.googleanalytics.isInternalSpecial(this.href)) {
          // Keep the internal URL for Google Analytics website overlay intact.
          ga("send", {
            "hitType": "pageview",
            "page": Drupal.googleanalytics.getPageUrl(this.href),
            "transport": "beacon"
          });
        }
      }
      else {
        if (Drupal.settings.googleanalytics.trackMailto && $(this).is("a[href^='mailto:'],area[href^='mailto:']")) {
          // Mailto link clicked.
          ga("send", {
            "hitType": "event",
            "eventCategory": "Mails",
            "eventAction": "Click",
            "eventLabel": this.href.substring(7),
            "transport": "beacon"
          });
        }
        else if (Drupal.settings.googleanalytics.trackOutbound && this.href.match(/^\w+:\/\//i)) {
          if (Drupal.settings.googleanalytics.trackDomainMode !== 2 || (Drupal.settings.googleanalytics.trackDomainMode === 2 && !Drupal.googleanalytics.isCrossDomain(this.hostname, Drupal.settings.googleanalytics.trackCrossDomains))) {
            // External link clicked / No top-level cross domain clicked.
            ga("send", {
              "hitType": "event",
              "eventCategory": "Outbound links",
              "eventAction": "Click",
              "eventLabel": this.href,
              "transport": "beacon"
            });
          }
        }
      }
    });
  });

  // Track hash changes as unique pageviews, if this option has been enabled.
  if (Drupal.settings.googleanalytics.trackUrlFragments) {
    window.onhashchange = function() {
      ga("send", {
        "hitType": "pageview",
        "page": location.pathname + location.search + location.hash
      });
    };
  }

  // Colorbox: This event triggers when the transition has completed and the
  // newly loaded content has been revealed.
  if (Drupal.settings.googleanalytics.trackColorbox) {
    $(document).bind("cbox_complete", function () {
      var href = $.colorbox.element().attr("href");
      if (href) {
        ga("send", {
          "hitType": "pageview",
          "page": Drupal.googleanalytics.getPageUrl(href)
        });
      }
    });
  }

});

/**
 * Check whether the hostname is part of the cross domains or not.
 *
 * @param string hostname
 *   The hostname of the clicked URL.
 * @param array crossDomains
 *   All cross domain hostnames as JS array.
 *
 * @return boolean
 */
Drupal.googleanalytics.isCrossDomain = function (hostname, crossDomains) {
  /**
   * jQuery < 1.6.3 bug: $.inArray crushes IE6 and Chrome if second argument is
   * `null` or `undefined`, http://bugs.jquery.com/ticket/10076,
   * https://github.com/jquery/jquery/commit/a839af034db2bd934e4d4fa6758a3fed8de74174
   *
   * @todo: Remove/Refactor in D8
   */
  if (!crossDomains) {
    return false;
  }
  else {
    return $.inArray(hostname, crossDomains) > -1 ? true : false;
  }
};

/**
 * Check whether this is a download URL or not.
 *
 * @param string url
 *   The web url to check.
 *
 * @return boolean
 */
Drupal.googleanalytics.isDownload = function (url) {
  var isDownload = new RegExp("\\.(" + Drupal.settings.googleanalytics.trackDownloadExtensions + ")([\?#].*)?$", "i");
  return isDownload.test(url);
};

/**
 * Check whether this is an absolute internal URL or not.
 *
 * @param string url
 *   The web url to check.
 *
 * @return boolean
 */
Drupal.googleanalytics.isInternal = function (url) {
  var isInternal = new RegExp("^(https?):\/\/" + window.location.host, "i");
  return isInternal.test(url);
};

/**
 * Check whether this is a special URL or not.
 *
 * URL types:
 *  - gotwo.module /go/* links.
 *
 * @param string url
 *   The web url to check.
 *
 * @return boolean
 */
Drupal.googleanalytics.isInternalSpecial = function (url) {
  var isInternalSpecial = new RegExp("(\/go\/.*)$", "i");
  return isInternalSpecial.test(url);
};

/**
 * Extract the relative internal URL from an absolute internal URL.
 *
 * Examples:
 * - http://mydomain.com/node/1 -> /node/1
 * - http://example.com/foo/bar -> http://example.com/foo/bar
 *
 * @param string url
 *   The web url to check.
 *
 * @return string
 *   Internal website URL
 */
Drupal.googleanalytics.getPageUrl = function (url) {
  var extractInternalUrl = new RegExp("^(https?):\/\/" + window.location.host, "i");
  return url.replace(extractInternalUrl, '');
};

/**
 * Extract the download file extension from the URL.
 *
 * @param string url
 *   The web url to check.
 *
 * @return string
 *   The file extension of the passed url. e.g. "zip", "txt"
 */
Drupal.googleanalytics.getDownloadExtension = function (url) {
  var extractDownloadextension = new RegExp("\\.(" + Drupal.settings.googleanalytics.trackDownloadExtensions + ")([\?#].*)?$", "i");
  var extension = extractDownloadextension.exec(url);
  return (extension === null) ? '' : extension[1];
};

})(jQuery);
;
