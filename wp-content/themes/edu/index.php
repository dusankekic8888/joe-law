<?php get_header(); ?>
<div id="slider">
	<?php echo do_shortcode('[rev_slider home_slider]');?>
</div>
<div class="intro">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<img src="<?php bloginfo('template_url');?>/images/lawyer.jpg">
			</div>
			<div class="col-md-8">
				<div class="intro-text">
					<h2>HIRE A LAWYER WHO IS USED TO WINNING!</h2>
					<p>With my core skills and life experience, I am better suited to assist either an individual or employer wishing to seek assistance to achieve their objective to secure its viability in the future. </p>
					<div class="readmore-button">
						<a href="">Read more</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<section class="section1">
	<div class="container">
		<div class="section1-box">
			<h2 class="title"><span>Our fees</span></h2>
			<p>
				We have a very competitive professional rates for our premium services both for corporate and individuals. if you would like to inquire about our rates, please contact (M)+61 425 412 072 or email us 
			</p>
		</div>
	</div>
</section>
<section class="section2">
	<div class="container"> 
		<h2 class="title"> <span>Our People</span></h2>
		<div class="row">
			<div class="col-md-6">
				<div class="left-section2-box first">
					<img src="<?php bloginfo('template_url');?>/images/lawyertbc.jpg">
					<p>Contact details</p>
					<p>M: +61 0425 412 072</p>
					<p>E: <span style="color: red">TBC</span></p>
					
				</div>
			</div>
			<div class="col-md-6">
				<div class="right-section2-box">
					<p>
						<b>Joe Aguilus, MBA, MBUS(IRHRM), GCertAusMigrationLawPrac</b>

					</p>

					<ul>
						<li>Founder and Director </li>
						<li>Registered Migration Agent </li>
						<li>Principal Industrial Relations and Human Resources Consultant </li>
						<li>Executive and Career Coach</li>
						<li>Australian Immigration (Both Temporary & Permanent Entry)</li>
						<li>Citizenship</li> 
						<li>Industrial Relations</li> 
						<li>Human Resources </li>
						<li>Recruitment </li>
						<li>Career Coaching </li>
					</ul>
				</div>
			</div>
		</div>

	</div>
</section>
<section class="section3">
	<div class="container">
		<h2>WINNING IS EASIER THAN YOU THINK…</h2>
		<p>
			The founder of Aguilus Solutions is Joe Aguilus. Joe has designed its business for people and/or businesses to have access to a reliable premium immigration and business consulting service that you can trust which is provided by highly educated, highly skilled and experienced professional who has a genuine desire helping people’s lives in our society. 
		</p>
		<div class="readmore-button">
			<a href="">Read more</a>
		</div>
	</div>
</section>
<section class="section4">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<div class="section4-box">
					<h2>Immigration and Citizenship Consulting</h2>
					<p>We will keep you updated with news and advice on civil, family and criminal advocacy issues.</p>
				</div>
			</div>
			<div class="col-md-4">
				<div class="section4-box">
					<h2>International Education Consulting</h2>
					<p>This deluxe service is only available to our clients. We may be able to offer this service in the future as we expand our business. </p>
				</div>
			</div>
			<div class="col-md-4">
				<div class="section4-box">
					<h2>International Education Consulting</h2>
					<p>This deluxe service is only available to our clients. We may be able to offer this service in the future as we expand our business. </p>
				</div>
			</div>
			<div class="col-md-4">
				<div class="section4-box">
					<h2>Career Coaching</h2>
					<p>This deluxe service is only available to our clients. We may be able to offer this service in the future as we expand our business. </p>
				</div>
			</div>
			<div class="col-md-4">
				<div class="section4-box">
					<h2>IELTS Test Preparation Services</h2>
					<p>This deluxe service is only available to our clients. We may be able to offer this service in the future as we expand our business. </p>
				</div>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>