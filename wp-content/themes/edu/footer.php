<div class="clear"></div>
<footer>
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<div class="top-footer-box">
					<h3>Contact</h3>
					<p>
						If you would like to book an appointment for consultant in relation to one of the services that we provide, please contact (M)+61 425 412 072. 
					</p>
				</div>
			</div>
			<div class="col-md-4">
				<div class="top-footer-box">
					<h3>
							Our opening hours:
					</h3>
					<p>
						Monday to Friday – 10:00AM to 5:00PM <br/>
						Saturday – 10:00AM to 1:00PM <br/>
						Except public holidays <br/>

						Please note: We provide site visit within 20km from Melbourne CBD. (Extra charge applies) 

					</p>
				</div>
			</div>
			<div class="col-md-4">
				<div class="top-footer-box">
					<h3>Social networks</h3>
					<ul class="social-media-links">
						<li>
							<a href="/" class="fa fa-2x fa-twitter noext" aria-hidden="true" target="_blank"><span>Visit us on Twitter</span><span class="accessibility"> (external link)</span></a>
						</li>
						<li>
							<a href="/" class="fa fa-2x fa-facebook-official noext" aria-hidden="true" target="_blank"><span>Visit us on Facebook</span><span class="accessibility"> (external link)</span></a>
						</li>
						<li>
							<a href="/" class="fa fa-2x fa-youtube noext" aria-hidden="true" target="_blank"><span>Visit us on Twitter Youtube</span><span class="accessibility"> (external link)</span></a>
						</li>
						<li>
							<a href="/" class="fa fa-2x fa-instagram noext" aria-hidden="true" target="_blank"><span>Visit us on Instagram</span><span class="accessibility"> (external link)</span></a>
						</li>
						<li>
							<a href="/" class="fa fa-2x fa-linkedin-square noext" aria-hidden="true" target="_blank"><span>Visit us on LinkedIn</span><span class="accessibility"> (external link)</span></a>
						</li>
						<li>
							<a href="/" class="fa fa-2x fa-google-plus noext" aria-hidden="true" target="_blank"><span>Visit us on Google Plus</span><span class="accessibility"> (external link)</span></a>
						</li>
					</ul>
				</div>
			</div>	
		</div>
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li><a href="#"> Sitemap </a></li>
						<li><a href="#"> Contact  </a></li>
						<li><a href="#"> Sponsors </a></li>
						<li><a href="#"> Privacy </a></li>
						<li><a href="#"> Disclaimer  </a></li>

					</ul>
				</div><!-- /.navbar-collapse -->
			</div><!-- /.container-fluid -->
		</nav>
		<p>© Aguilus Solutions 2017 </p>
	</div>
</footer>

<?php wp_footer(); ?>
</body>
</html>