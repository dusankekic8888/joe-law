<?php 
	class Popular_Posts extends WP_Widget {

		function __construct() {
			parent::__construct(
				'popular_posts', // ID
				__( 'Bài viết được quan tâm', 'willgroup' ), // Name
				array( 'description' => __( 'Widget hiển thị những bài viết được quan tâm', 'willgroup' ), ) // Args
			);
		}

		public function widget( $args, $instance ) {
			$title = $instance['title'];
			$number_post = $instance['number_post'];
			
			$query_args = array(
				'posts_per_page' => $number_post,
				'meta_key' 		 => 'post_views',
				'orderby' 		 => 'meta_value_num',
			); 
			$query = new WP_Query( $query_args ); 
			if ( $query->have_posts() ) {
				echo $args['before_widget'];
					if ( $title != '' ) {
						echo $args['before_title'] . apply_filters( 'widget_title', $title ) . $args['after_title'];
					} ?>
					<div class="widget-content">
						<?php while ( $query->have_posts() ) : $query->the_post(); ?>
							<article class="float-news aside-news">
								<a class="image" href="<?php the_permalink(); ?>"><?php the_post_thumbnail('thumbnail'); ?></a>
								<h4 class="name"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
							</article>
						<?php endwhile; wp_reset_postdata(); ?>
					</div>
				<?php
				echo $args['after_widget'];				
			}
		}

		/* Back-end widget form.
		   @param array $instance Previously saved values from database.
		*/
		public function form( $instance ) {
			$title = $instance['title'];
			$number_post = $instance['number_post'] != '' ? $instance['number_post'] : 5;
			?>
			
			<p>
				<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Tiêu đề:', 'willgroup' ); ?></label> 
				<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
			</p>
			<p>
				<label for="<?php echo $this->get_field_id( 'number_post' ); ?>"><?php _e( 'Số lượng:', 'willgroup' ); ?></label>
				<input class="tiny-text" type="number" size="3" value="<?php echo esc_attr( $number_post ); ?>" min="1" step="1" name="<?php echo $this->get_field_name( 'number_post' ); ?>" id="<?php echo $this->get_field_id( 'number_post' ); ?>">
			</p>
			
			<?php
		}

		public function update( $new_instance, $old_instance ) {
			$instance = $old_instance;
			$instance['title'] = strip_tags( $new_instance['title'] );
			$instance['number_post'] = strip_tags( $new_instance['number_post'] );
			return $instance;
		}

	}
	// register Popular_Posts widget
	function register_popular_posts() {
		register_widget( 'Popular_Posts' );
	}
	add_action( 'widgets_init', 'register_popular_posts' );
?>