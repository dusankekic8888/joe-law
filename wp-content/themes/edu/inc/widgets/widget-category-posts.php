<?php 
	class Category_Posts extends WP_Widget {

		function __construct() {
			parent::__construct(
				'category_posts', // ID
				__( 'Bài viết chuyên mục', 'willgroup' ), // Name
				array( 'description' => __( 'Widget hiển thị những post của 1 chuyên mục', 'willgroup' ), ) // Args
			);
		}

		public function widget( $args, $instance ) {
			$category = $instance['category'];
			$number_post = $instance['number_post'];
			
			$query_args = array(
				'posts_per_page' => $number_post,
				'cat'			 => $category,
			);
			$query = new WP_Query( $query_args );
			if ( $query->have_posts() ) {
				echo $args['before_widget'];
					echo $args['before_title'] . '<a href="' . get_category_link( $category ) . '">' . apply_filters( 'widget_title', get_cat_name( $category ) ) . '</a>' . $args['after_title']; ?>
					<div class="widget-content">
						<?php while ( $query->have_posts() ) : $query->the_post(); ?>
							<article class="float-news aside-news">
								<a class="image" href="<?php the_permalink(); ?>"><?php the_post_thumbnail('thumbnail'); ?></a>
								<h4 class="name"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
							</article>
						<?php endwhile; wp_reset_postdata(); ?>
					</div>
				<?php
				echo $args['after_widget'];
			}
		}

		/* Back-end widget form.
		   @param array $instance Previously saved values from database.
		*/
		public function form( $instance ) {
			$category = $instance['category'];
			$number_post = $instance['number_post'] != '' ? $instance['number_post'] : 5;
			?>
			
			<?php $terms = get_terms( 'category', array('hide_empty' => true, 'hierarchical' => false) ); if( $terms != '' && !is_wp_error( $terms ) ) { ?>
			<p>
				<label for="<?php echo $this->get_field_id( 'category' ); ?>"><?php _e( 'Thể loại:', $GLOBALS['text_domain'] ); ?></label> 
				<select class="widefat" id="<?php echo $this->get_field_id( 'category' ); ?>" name="<?php echo $this->get_field_name( 'category' ); ?>">
					<option value="0">Chọn thể loại</option>
					<?php foreach( $terms as $term ) { ?>
					<option value="<?php echo esc_attr( $term->term_id ); ?>" <?php selected( $category, $term->term_id ); ?>><?php echo $term->name; ?> (<?php echo $term->count; ?>)</option>
					<?php } ?>
				</select>
			</p>
			<?php } ?>
			<p>
				<label for="<?php echo $this->get_field_id( 'number_post' ); ?>"><?php _e( 'Số bài viết:', $GLOBALS['text_domain'] ); ?></label>
				<input class="tiny-text" type="number" size="3" value="<?php echo esc_attr( $number_post ); ?>" min="1" step="1" name="<?php echo $this->get_field_name( 'number_post' ); ?>" id="<?php echo $this->get_field_id( 'number_post' ); ?>">
			</p>
			
			<?php
		}

		public function update( $new_instance, $old_instance ) {
			$instance = $old_instance;
			$instance['category'] = strip_tags( $new_instance['category'] );
			$instance['number_post'] = strip_tags( $new_instance['number_post'] );
			return $instance;
		}

	}
	// register Category_Posts widget
	function register_category_posts() {
		register_widget( 'Category_Posts' );
	}
	add_action( 'widgets_init', 'register_category_posts' );
?>