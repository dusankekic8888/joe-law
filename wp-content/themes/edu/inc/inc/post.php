<?php
	/**
	 * Add post field group
	 */
	if( function_exists('acf_add_local_field_group') ) {
		
		acf_add_local_field_group(	array(
			'key'		=> 'post_settings',
			'title' 	=> __( 'Thông tin', 'willgroup' ),
			'fields' 	=> array (
				array (
					'key'   		=> 'post_views',
					'label' 		=> __( 'Lượt xem', 'willgroup' ),
					'name'  		=> 'post_views',
					'type'  		=> 'number',
					'default_value' => 0,
					'step' 			=> 1,
				),
			),
			'location' => array (
				array (
					array (
						'param'    => 'post_type',
						'operator' => '==',
						'value'    => 'post',
					),
				),
			),
			'position' => 'normal',
			'label_placement' => 'left',
		));
	}
	
	/**
	 * Add post columns
	 */
	function willgroup_columns_head($cols) {		
		$cols = array_merge(
			array_slice( $cols, 0, 1, true ),
			array( 'thumbnail' => __( 'Ảnh' , 'willgroup' ) ),
			array_slice( $cols, 1, null, true )
		);
		return $cols;
	}
	function willgroup_columns_content($col_name, $post_ID) {
		if ($col_name == 'thumbnail') {
			$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post_ID ), 'thumbnail' );
			if ( $image ) {
				echo '<a href="' . get_edit_post_link( $post_ID ) . '"><img width="75px" src="' . $image['0'] . '"/></a>';
			} else {
				echo '—';
			}
			echo '<style type="text/css">';
			echo '.column-thumbnail { width:70px; }';
			echo '</style>';
		}
	}
	add_filter('manage_post_posts_columns', 'willgroup_columns_head');
	add_action('manage_post_posts_custom_column', 'willgroup_columns_content', 10, 2);
?>