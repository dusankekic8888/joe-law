<?php
/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function willgroup_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'willgroup' ),
		'id'            => 'sidebar',
		'description'   => esc_html__( 'Add widgets here.', 'willgroup' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar affix', 'willgroup' ),
		'id'            => 'sidebar-affix',
		'description'   => esc_html__( 'Add widgets here.', 'willgroup' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
}
add_action( 'widgets_init', 'willgroup_widgets_init' );