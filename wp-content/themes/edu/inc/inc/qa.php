<?php
// Add qa post type
register_post_type( 'qa',
	array ( 
		'labels' => array(
			'name' 				=> __( 'Hỏi đáp', 'willgroup' ),
			'singular_name' 	=> __( 'Hỏi đáp', 'willgroup' ),
			'menu_name' 		=> __( 'Hỏi đáp', 'willgroup' ),
			'name_admin_bar'    => __( 'Hỏi đáp', 'willgroup' ),
			'all_items'			=> __( 'Tất cả Hỏi đáp', 'willgroup' ),
			'add_new' 			=> __( 'Thêm Hỏi đáp', 'willgroup' ),
			'add_new_item' 		=> __( 'Thêm Hỏi đáp', 'willgroup' ),
			'edit_item' 		=> __( 'Edit Hỏi đáp', 'willgroup' ),
		),
		'description' 		=> __( 'Hỏi đáp', 'willgroup' ),
		'menu_position' 	=> 5,
		'menu_icon' 		=> 'dashicons-editor-help',
		'capability_type' 	=> 'post',
		'public' 			=> true,
		'has_archive' 		=> true,
		'supports' 			=> array('title'),
		'taxonomies' 		=> array(),
		'rewrite' 			=> array( 'slug' => 'hoi-dap', 'with_front' => true ),
	)
);


/**
 * Add qa field group
 */
if( function_exists('acf_add_local_field_group') ) {
	
	acf_add_local_field_group(	array(
		'key'		=> 'qa_settings',
		'title' 	=> __( 'Thông tin', 'willgroup' ),
		'fields' 	=> array (
			array (
				'key'   		=> 'qa_name',
				'label' 		=> __( 'Họ và tên', 'willgroup' ),
				'name'  		=> 'qa_name',
				'type'  		=> 'text',
			),
			array (
				'key'   		=> 'qa_email',
				'label' 		=> __( 'Email', 'willgroup' ),
				'name'  		=> 'qa_email',
				'type'  		=> 'email',
			),
			array (
				'key'   		=> 'qa_question',
				'label' 		=> __( 'Câu hỏi', 'willgroup' ),
				'name'  		=> 'qa_question',
				'type'  		=> 'textarea',
			),
			array (
				'key'   		=> 'qa_answer',
				'label' 		=> __( 'Câu trả lời', 'willgroup' ),
				'name'  		=> 'qa_answer',
				'type'  		=> 'wysiwyg',
			),
		),
		'location' => array (
			array (
				array (
					'param'    => 'post_type',
					'operator' => '==',
					'value'    => 'qa',
				),
			),
		),
		'position' => 'normal',
		'label_placement' => 'left',
	));
}
// Add qa columns
function willgroup_qa_columns_head($cols) {		
	$cols = array_merge(
		array_slice( $cols, 0, 2, true ),
		array( 'name' => __( 'Họ và tên' , 'willgroup' ) ),
		array( 'email'  => __( 'Email' , 'willgroup' ) ),
		array_slice( $cols, 2, null, true )
	);
	return $cols;
}
function willgroup_qa_columns_content($col_name, $post_ID) {
	if ($col_name == 'name') {
		$value = get_field( 'qa_name', $post_ID );
		if ( $value ) {
			echo $value;
		} else {
			echo '—';
		}
	}
	if ($col_name == 'email') {
		$value = get_field( 'qa_email', $post_ID );
		if ( $value ) {
			echo $value;
		} else {
			echo '—';
		}
	}
}
add_filter('manage_qa_posts_columns', 'willgroup_qa_columns_head');
add_action('manage_qa_posts_custom_column', 'willgroup_qa_columns_content', 10, 2);