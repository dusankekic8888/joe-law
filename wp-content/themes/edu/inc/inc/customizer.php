<?php
/**
 * willgroup Theme Customizer.
 *
 * @package willgroup
 */

/**
 * Create options page
 */
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> 'Tùy chỉnh theme',
		'menu_title'	=> 'Tùy chỉnh theme',
		'menu_slug' 	=> 'customizer',
		'capability'	=> 'edit_posts',
		'icon_url' 		=> 'dashicons-hammer',
		'id'			=> 'customizer',
		'post_id' 		=> 'customizer',
	));
}

/**
 * Add FTP field group
 */
if( function_exists('acf_add_local_field_group') ) {
	
	acf_add_local_field_group(	array(
		'key'		=> 'customizer_setup',
		'title' 	=> __( 'Cài đặt', 'willgroup' ),
		'fields' 	=> array (
			array (
				'key'   		=> 'tab_header',
				'label' 		=> __( 'Header', 'willgroup' ),
				'name'  		=> 'tab_header',
				'type'  		=> 'tab',
				'placement' 	=> 'left',
			),
			array (
				'key'   		=> 'logo',
				'label' 		=> __( 'Logo', 'willgroup' ),
				'name'  		=> 'logo',
				'type'  		=> 'image',
			),
			array (
				'key'   		=> 'title_1',
				'label' 		=> __( 'Tiêu đề 1', 'willgroup' ),
				'name'  		=> 'title_1',
				'type'  		=> 'text',
			),
			array (
				'key'   		=> 'title_2',
				'label' 		=> __( 'Tiêu đề 2', 'willgroup' ),
				'name'  		=> 'title_2',
				'type'  		=> 'text',
			),
			array (
				'key'   		=> 'favicon',
				'label' 		=> __( 'Favicon', 'willgroup' ),
				'name'  		=> 'favicon',
				'type'  		=> 'image',
			),
			array (
				'key'   		=> 'hotline',
				'label' 		=> __( 'Hotline', 'willgroup' ),
				'name'  		=> 'hotline',
				'type'  		=> 'text',
			),
			array (
				'key'   		=> 'tab_slides',
				'label' 		=> __( 'Slides', 'willgroup' ),
				'name'  		=> 'tab_slides',
				'type'  		=> 'tab',
				'placement' 	=> 'left',
			),
			array (
				'key' 		   => 'slides',
				'label'		   => __( 'Slides', 'willgroup' ),
				'name' 		   => 'slides',
				'type' 		   => 'repeater',
				'layout'	   => 'table',
				'button_label' => __( 'Thêm', 'willgroup' ),
				'sub_fields' => array (
					array (
						'key' 	=> 'image',
						'label' => __( 'Hình ảnh', 'willgroup' ),
						'name' 	=> 'image',
						'type' 	=> 'image',
					),
					array (
						'key' 	=> 'url',
						'label' => __( 'Liên kết', 'willgroup' ),
						'name' 	=> 'url',
						'type' 	=> 'url',
					),
				),
			),
			array (
				'key'   		=> 'tab_home',
				'label' 		=> __( 'Trang chủ', 'willgroup' ),
				'name'  		=> 'tab_home',
				'type'  		=> 'tab',
				'placement' 	=> 'left',
			),
			array (
				'key'   		=> 'home_cats',
				'label' 		=> __( 'Những thể loại hiển thị ở trang chủ', 'willgroup' ),
				'name'  		=> 'home_cats',
				'type'  		=> 'taxonomy',
				'taxonomy'		=> 'category',
				'field_type' 	=> 'multi_select',
				'return_format' => 'object',
			),
			array (
				'key' 	=> 'home_image_1',
				'label' => __( 'Hình ảnh 1', 'willgroup' ),
				'name' 	=> 'home_image_1',
				'type' 	=> 'image',
			),
			array (
				'key' 	=> 'home_url_1',
				'label' => __( 'Liên kết 1', 'willgroup' ),
				'name' 	=> 'home_url_1',
				'type' 	=> 'text',
			),
			array (
				'key' 	=> 'home_image_2',
				'label' => __( 'Hình ảnh 2', 'willgroup' ),
				'name' 	=> 'home_image_2',
				'type' 	=> 'image',
			),
			array (
				'key' 	=> 'home_url_2',
				'label' => __( 'Liên kết 2', 'willgroup' ),
				'name' 	=> 'home_url_2',
				'type' 	=> 'text',
			),
			array (
				'key'   		=> 'tab_archive',
				'label' 		=> __( 'Danh mục', 'willgroup' ),
				'name'  		=> 'tab_archive',
				'type'  		=> 'tab',
				'placement' 	=> 'left',
			),
			array (
				'key'   		=> 'archive_cats',
				'label' 		=> __( 'Những thể loại hiển thị ở trang danh mục', 'willgroup' ),
				'name'  		=> 'archive_cats',
				'type'  		=> 'taxonomy',
				'taxonomy'		=> 'category',
				'field_type' 	=> 'multi_select',
				'return_format' => 'object',
			),
			array (
				'key'   		=> 'tab_post',
				'label' 		=> __( 'Bài viết', 'willgroup' ),
				'name'  		=> 'tab_post',
				'type'  		=> 'tab',
				'placement' 	=> 'left',
			),
			array(
				'key'        => 'hotline_image',
				'label'      => __( 'Hình ảnh hotline', 'willgroup' ),
				'name'       => 'hotline_image',
				'type'       => 'image',
			),
			array(
				'key'        => 'hotline_link',
				'label'      => __( 'Liên kết hotline', 'willgroup' ),
				'name'       => 'hotline_link',
				'type'       => 'text',
			),
			array(
				'key'        => 'order_image',
				'label'      => __( 'Hình ảnh đặt mua', 'willgroup' ),
				'name'       => 'order_image',
				'type'       => 'image',
			),
			array(
				'key'        => 'order_link',
				'label'      => __( 'Liên kết đặt mua', 'willgroup' ),
				'name'       => 'order_link',
				'type'       => 'text',
			),
			array (
				'key'   		=> 'tab_footer',
				'label' 		=> __( 'Footer', 'willgroup' ),
				'name'  		=> 'tab_footer',
				'type'  		=> 'tab',
				'placement' 	=> 'left',
			),
			array (
				'key'   		=> 'footer_logo',
				'label' 		=> __( 'Logo công ty', 'willgroup' ),
				'name'  		=> 'footer_logo',
				'type'  		=> 'image',
			),
			array (
				'key'   		=> 'footer_info',
				'label' 		=> __( 'Thông tin', 'willgroup' ),
				'name'  		=> 'footer_info',
				'type'  		=> 'wysiwyg',
			),
			array (
				'key'   		=> 'footer_desc',
				'label' 		=> __( 'Mô tả', 'willgroup' ),
				'name'  		=> 'footer_desc',
				'type'  		=> 'wysiwyg',
			),
			array (
				'key'   		=> 'footer_copyright',
				'label' 		=> __( 'Copyright', 'willgroup' ),
				'name'  		=> 'footer_copyright',
				'type'  		=> 'text',
			),
		),
		'location' => array (
			array (
				array (
					'param'    => 'options_page',
					'operator' => '==',
					'value'    => 'customizer',
				),
			),
		),
	));
}