<?php
/**
 * Insert comment
 */
function king_insert_comment() {
	$comment = $_POST['comment'];
	$name = $_POST['name'];
	$email = $_POST['email'];
	$post_id = $_POST['post_id'];
	$parent = $_POST['parent'];
	$parent = $parent ? $parent : 0;
	
	header('Content-Type: application/json');
	if ( $comment == '' ) {
		echo json_encode( array( 'status' => false, 'message' => __( 'Bạn chưa nhập bình luận.', 'willgroup' ) ) );
		die();
	}
	if ( $name == '' ) {
		echo json_encode( array( 'status' => false, 'message' => __( 'Bạn chưa nhập tên.', 'willgroup' ) ) );
		die();
	}
	if ( $email == '' ) {
		echo json_encode( array( 'status' => false, 'message' => __( 'Bạn chưa nhập email.', 'willgroup' ) ) );
		die();
	}
	if ( !is_email( $_POST['email'] ) ) {
		echo json_encode( array( 'status' => false, 'message' => __( 'Email không hợp lệ.', 'willgroup' ) ) );
		die();
	}
		
	$comment_id = wp_new_comment( array(
					  'comment_post_ID' 	 => $post_id, 
					  'comment_author' 		 => $name, 
					  'comment_author_email' => $email, 
					  'comment_content' 	 => $comment, 
					  'comment_parent' 		 => $parent,
				  ) );
	wp_set_comment_status( $comment_id, 'approve' );
	
	echo json_encode( array( 'status' => true, 'message' => __( 'Gửi bình luận thành công.', 'willgroup' ) ) );
	die();
}
add_action( 'wp_ajax_nopriv_king_insert_comment', 'king_insert_comment' );
add_action( 'wp_ajax_king_insert_comment', 'king_insert_comment' );

/**
 * Get phone
 */
function willgroup_get_phone() {
	$name = $_POST['name'];
	$phone = $_POST['phone'];
	$note = $_POST['note'];
	
	$regex = "/^(\d[\s-]?)?[\(\[\s-]{0,2}?\d{3}[\)\]\s-]{0,2}?\d{3}[\s-]?\d{4}$/i";
	
	if ( $name == '' ) {
		die( __( 'Bạn chưa nhập tên.', 'willgroup' ) );
	}
	if ( $phone == '' ) {
		die( __( 'Bạn chưa nhập số điện thoại.', 'willgroup' ) );
	}
	if ( ! is_numeric ( $phone ) ) {
		die( __( 'Số điện thoại chỉ bao gồm những số.', 'willgroup' ) );
	}
	if ( strlen( $phone ) < 10 ) {
		die( __( 'Số điện thoại phải có nhiều hơn 9 số.', 'willgroup' ) );
	}
		
	wp_insert_post( array(
		'post_type'    => 'consultant',
		'post_status'  => 'publish',
		'post_title'   => $name,
		'meta_input'   => array(
			'consultant_phone'    => $phone,
			'consultant_note'     => $note,
		)
	) );
	
	die( __( 'Gửi yêu cầu thành công.', 'willgroup' ) );
}
add_action( 'wp_ajax_nopriv_willgroup_get_phone', 'willgroup_get_phone' );
add_action( 'wp_ajax_willgroup_get_phone', 'willgroup_get_phone' );

/**
 * Get send qa
 */
function willgroup_send_qa() {
	$name = $_POST['name'];
	$email = $_POST['email'];
	$title = $_POST['title'];
	$question = $_POST['question'];
	
	if ( $name == '' ) {
		die( __( 'Bạn chưa nhập tên.', 'willgroup' ) );
	}
	if ( $email == '' ) {
		die( __( 'Bạn chưa nhập email.', 'willgroup' ) );
	}
	if ( !is_email( $_POST['email'] ) ) {
		die( __( 'Email không hợp lệ.', 'willgroup' ) );
	}
	if ( $title == '' ) {
		die( __( 'Bạn chưa nhập tiêu đề.', 'willgroup' ) );
	}
	if ( $question == '' ) {
		die( __( 'Bạn chưa nhập câu hỏi.', 'willgroup' ) );
	}
		
	wp_insert_post( array(
		'post_type'    => 'qa',
		'post_status'  => 'publish',
		'post_title'   => $title,
		'meta_input'   => array(
			'qa_name'       => $name,
			'qa_email'      => $email,
			'qa_question'   => $question,
		)
	) );
	
	die( __( 'Gửi câu hỏi thành công.', 'willgroup' ) );
}
add_action( 'wp_ajax_nopriv_willgroup_send_qa', 'willgroup_send_qa' );
add_action( 'wp_ajax_willgroup_send_qa', 'willgroup_send_qa' );