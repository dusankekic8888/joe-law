<?php
/**
 * Truncate text.
 */
function truncate( $text, $chars = 300 ) {
	$text = strip_tags( $text );
	if ( strlen( $text ) > $chars ) {
		$text = $text." ";
		$text = substr($text,0,$chars);
		$text = substr($text,0,strrpos($text,' '));
		$text = $text."...";
	}
    return $text;
}

/**
 * Resize image.
 */
function resize_image( $image_width, $image_height ) {
	global $post;
	$params = array( 'width' => $image_width, 'height' => $image_height );
	$imgsrc = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID, '' ), 'full' );
	$custom_img_src = bfi_thumb( $imgsrc[0], $params );
	return $custom_img_src;
}

/**
 * Make links clickable
 */
function make_links_clickable($text){
    return preg_replace('!(((f|ht)tp(s)?://)[-a-zA-Zа-яА-Я()0-9@:%_+.~#?&;//=]+)!i', '<a href="$1">$1</a>', $text);
}
