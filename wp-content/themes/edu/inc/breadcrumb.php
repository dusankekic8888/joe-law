<?php
// Breadcrumbs
function willgroup_breadcrumb() {
       
    // Settings
    $breadcrums_id      = 'breadcrumb';
    $breadcrums_class   = 'breadcrumb';
    $home_title         = __( 'Trang chủ', 'willgroup' );
      
    // If you have any custom post types with custom taxonomies, put the taxonomy name below (e.g. product_cat)
    $custom_taxonomy    = 'product_cat';
       
    // Get the query & post information
    global $post,$wp_query;
       
    // Do not display on the homepage
    if ( !is_front_page() ) {
    ?>
	   
	<ul id="<?php echo $breadcrums_id; ?>" class="<?php echo $breadcrums_class; ?>">           
        <li class="breadcrumb-item home"><a href="<?php echo get_home_url(); ?>" title="<?php echo $home_title; ?>"><?php echo $home_title; ?></a></li>
          
		<?php	
        if ( is_archive() && !is_tax() && !is_category() && !is_tag() ) {
        ?>
        <li class="breadcrumb-item active"><?php echo post_type_archive_title($prefix, false); ?></li>
        <?php    
        } else if ( is_archive() && is_tax() && !is_category() && !is_tag() ) {
              
            // If post is a custom post type
            $post_type = get_post_type();
              
            // If it is a custom post type display name and link
            if($post_type != 'post') {
                  
                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);
				?>
                <li class="breadcrumb-item"><a href="<?php echo $post_type_archive; ?>" title="<?php echo $post_type_object->labels->name; ?>"><?php echo $post_type_object->labels->name; ?></a></li>
				<?php
            }
			$custom_tax_name = get_queried_object()->name;
			?>
            <li class="breadcrumb-item active"><?php echo $custom_tax_name; ?></li>
        <?php
        } else if ( is_single() ) {
              
            // If post is a custom post type
            $post_type = get_post_type();
              
            // If it is a custom post type display name and link
            if($post_type != 'post') {
                  
                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);
				?>
                <li class="breadcrumb-item"><a href="<?php echo $post_type_archive; ?>" title="<?php echo $post_type_object->labels->name; ?>"><?php echo $post_type_object->labels->name; ?></a></li>
				<?php
            }
              
            // Get post category info
            $category = get_the_category();
             
            if(!empty($category)) {
              
                // Get last category post is in
                $last_category = end(array_values($category));
                  
                // Get parent any categories and create array
                $get_cat_parents = rtrim(get_category_parents($last_category->term_id, true, ','),',');
                $cat_parents = explode(',',$get_cat_parents);
                  
                // Loop through parent categories and store in variable $cat_display
                $cat_display = '';
                foreach($cat_parents as $parents) {
                    $cat_display .= '<li class="breadcrumb-item item-cat">'.$parents.'</li>';
                }
             
            }
              
            // If it's a custom post type within a custom taxonomy
            $taxonomy_exists = taxonomy_exists($custom_taxonomy);
            if(empty($last_category) && !empty($custom_taxonomy) && $taxonomy_exists) {
                   
                $taxonomy_terms = get_the_terms( $post->ID, $custom_taxonomy );
                $cat_id         = $taxonomy_terms[0]->term_id;
                $cat_nicename   = $taxonomy_terms[0]->slug;
                $cat_link       = get_term_link($taxonomy_terms[0]->term_id, $custom_taxonomy);
                $cat_name       = $taxonomy_terms[0]->name;
               
            }
              
            // Check if the post is in a category
            if(!empty($last_category)) {
                echo $cat_display;
			?>
                <!--<li class="breadcrumb-item active"><?php echo get_the_title(); ?></li>-->
			<?php      
            // Else if post is in a custom taxonomy
            } else if(!empty($cat_id)) {
			?>  
                <li class="breadcrumb-item"><a href="<?php echo $cat_link; ?>" title="<?php echo $cat_name; ?>"><?php echo $cat_name; ?></a></li>
                <!--<li class="breadcrumb-item active"><?php echo get_the_title(); ?></li>-->
			<?php
            } else {
			?>
                <!--<li class="breadcrumb-item active"><?php echo get_the_title(); ?></li>-->
			<?php    
            }
              
        } else if ( is_category() ) { 
            // Category page
		?>
            <li class="breadcrumb-item active"><?php echo single_cat_title('', false); ?></li>
               
        <?php
		} else if ( is_page() ) {
               
            // Standard page
            if( $post->post_parent ){
                   
                // If child page, get parents 
                $anc = get_post_ancestors( $post->ID );
                   
                // Get parents in the right order
                $anc = array_reverse($anc);
                   
                // Parent page loop
                foreach ( $anc as $ancestor ) {
				?>
                    <li class="breadcrumb-item"><a href="<?php echo get_permalink($ancestor); ?>" title="<?php echo get_the_title($ancestor); ?>"><?php echo get_the_title($ancestor); ?></a></li>
                <?php
				}
                   
                // Display parent pages
                echo $parents;
                   
                // Current page
				?>
                <li class="breadcrumb-item active"><?php echo get_the_title(); ?></li>
                <?php   
            } else {
                   
                // Just display current page if not parents
			?>
                <li class="breadcrumb-item active"><?php echo get_the_title(); ?></li>
            <?php 
            }
               
        } else if ( is_tag() ) {
               
            // Tag page
               
            // Get tag information
            $term_id        = get_query_var('tag_id');
            $taxonomy       = 'post_tag';
            $args           = 'include=' . $term_id;
            $terms          = get_terms( $taxonomy, $args );
            $get_term_id    = $terms[0]->term_id;
            $get_term_slug  = $terms[0]->slug;
            $get_term_name  = $terms[0]->name;
               
            // Display the tag name
			?>
            <li class="breadcrumb-item active"><?php echo __( 'Từ khóa', 'willgroup' ) . ': ' . $get_term_name; ?></li>
        <?php   
        } elseif ( is_day() ) {
               
            // Day archive
		?>
            <li class="breadcrumb-item"><a href="<?php echo get_year_link( get_the_time('Y') ); ?>" title="<?php echo get_the_time('Y'); ?>"><?php echo get_the_time('Y'); ?></a></li>
            <li class="breadcrumb-item"><a href="<?php echo get_month_link( get_the_time('Y'), get_the_time('m') ); ?>" title="<?php echo get_the_time('M'); ?>"><?php echo get_the_time('M'); ?></a></li>
            <li class="breadcrumb-item active"><?php echo get_the_time('jS') . ' ' . get_the_time('M'); ?></li>
        <?php 
        } else if ( is_month() ) {
               
            // Month Archive
        ?>
            <li class="breadcrumb-item"><a href="<?php echo get_year_link( get_the_time('Y') ); ?>" title="<?php echo get_the_time('Y'); ?>"><?php echo get_the_time('Y'); ?></a></li>
            <li class="breadcrumb-item active"><?php echo get_the_time('M'); ?></li>
        <?php     
        } else if ( is_year() ) {
            // Display year archive
		?>
            <li class="breadcrumb-item active"><?php echo get_the_time('Y'); ?></li>
        <?php     
        } else if ( is_author() ) {
               
            // Auhor archive
               
            // Get the author information
            global $author;
            $userdata = get_userdata( $author );
               
            // Display author name
		?>
            <li class="breadcrumb-item active"><?php echo __( 'Author', 'willgroup' ) . ': ' . $userdata->display_name; ?></li>
        <?php
        } else if ( get_query_var('paged') ) {
               
            // Paginated archives
		?>
            <li class="breadcrumb-item active"><?php echo __( 'Page', 'willgroup' ) . ' ' . get_query_var('paged'); ?></li>
        <?php     
        } else if ( is_search() ) {
           
            // Search results page
		?>
            <li class="breadcrumb-item active"><?php echo __( 'Kết quả tìm kiếm cho', 'willgroup' ) . ': ' . get_search_query(); ?></li>
        <?php 
        } elseif ( is_404() ) {
               
            // 404 page
		?>
            <li class="breadcrumb-item"><?php _e( 'Lỗi 404', 'willgroup' ); ?></li>
		<?php
        }
		?>
	</ul>
	<?php
    }     
}
?>