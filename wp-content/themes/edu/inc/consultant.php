<?php
// Add consultant post type
register_post_type( 'consultant',
	array ( 
		'labels' => array(
			'name' 				=> __( 'Đặt hàng', 'willgroup' ),
			'singular_name' 	=> __( 'Đặt hàng', 'willgroup' ),
			'menu_name' 		=> __( 'Đặt hàng', 'willgroup' ),
			'name_admin_bar'    => __( 'Đặt hàng', 'willgroup' ),
			'all_items'			=> __( 'Tất cả Đặt hàng', 'willgroup' ),
			'add_new' 			=> __( 'Thêm Đặt hàng', 'willgroup' ),
			'add_new_item' 		=> __( 'Thêm Đặt hàng', 'willgroup' ),
			'edit_item' 		=> __( 'Edit Đặt hàng', 'willgroup' ),
		),
		'description' 		=> __( 'Đặt hàng', 'willgroup' ),
		'menu_position' 	=> 5,
		'menu_icon' 		=> 'dashicons-format-chat',
		'capability_type' 	=> 'post',
		'public' 			=> true,
		'has_archive' 		=> true,
		'supports' 			=> array('title'),
		'taxonomies' 		=> array(),
		'rewrite' 			=> array( 'slug' => 'dat-hang', 'with_front' => true ),
	)
);


/**
 * Add consultant field group
 */
if( function_exists('acf_add_local_field_group') ) {
	
	acf_add_local_field_group(	array(
		'key'		=> 'consultant_settings',
		'title' 	=> __( 'Thông tin', 'willgroup' ),
		'fields' 	=> array (
			array (
				'key'   		=> 'consultant_phone',
				'label' 		=> __( 'Điện thoại:', 'willgroup' ),
				'name'  		=> 'consultant_phone',
				'type'  		=> 'text',
			),
			array (
				'key'   		=> 'consultant_note',
				'label' 		=> __( 'Địa chỉ:', 'willgroup' ),
				'name'  		=> 'consultant_note',
				'type'  		=> 'textarea',
			),
		),
		'location' => array (
			array (
				array (
					'param'    => 'post_type',
					'operator' => '==',
					'value'    => 'consultant',
				),
			),
		),
		'position' => 'normal',
		'label_placement' => 'left',
	));
}
// Add consultant columns
function willgroup_consultant_columns_head($cols) {		
	$cols = array_merge(
		array_slice( $cols, 0, 2, true ),
		array( 'phone' => __( 'Điện thoại' , 'willgroup' ) ),
		array( 'note'  => __( 'Địa chỉ' , 'willgroup' ) ),
		array_slice( $cols, 2, null, true )
	);
	return $cols;
}
function willgroup_consultant_columns_content($col_name, $post_ID) {
	if ($col_name == 'phone') {
		$phone = get_field( 'consultant_phone', $post_ID );
		if ( $phone ) {
			echo $phone;
		} else {
			echo '—';
		}
	}
	if ($col_name == 'note') {
		$note = get_field( 'consultant_note', $post_ID );
		if ( $note ) {
			echo $note;
		} else {
			echo '—';
		}
	}
}
add_filter('manage_consultant_posts_columns', 'willgroup_consultant_columns_head');
add_action('manage_consultant_posts_custom_column', 'willgroup_consultant_columns_content', 10, 2);