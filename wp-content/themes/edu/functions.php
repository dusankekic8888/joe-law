<?php
add_action( 'after_setup_theme', 'blankslate_setup' );
function blankslate_setup()
{
    load_theme_textdomain( 'blankslate', get_template_directory() . '/languages' );
    add_theme_support( 'title-tag' );
    add_theme_support( 'automatic-feed-links' );
    add_theme_support( 'post-thumbnails' );
    global $content_width;
    if ( ! isset( $content_width ) ) $content_width = 640;
    register_nav_menus(
        array( 
           'main-menu' => __( 'Main Menu', 'blankslate' ),
           'category-sidebar' => __( 'Category Sidebar', 'blankslate' ),
           'mobilemenu' => __( 'Mobile Menu', 'blankslate' ),
           )
        );
}
add_action( 'comment_form_before', 'blankslate_enqueue_comment_reply_script' );
function blankslate_enqueue_comment_reply_script()
{
    if ( get_option( 'thread_comments' ) ) { wp_enqueue_script( 'comment-reply' ); }
}
add_filter( 'the_title', 'blankslate_title' );
function blankslate_title( $title ) {
    if ( $title == '' ) {
        return '&rarr;';
    } else {
        return $title;
    }
}
add_filter( 'wp_title', 'blankslate_filter_wp_title' );
function blankslate_filter_wp_title( $title )
{
    return $title . esc_attr( get_bloginfo( 'name' ) );
}
add_action( 'widgets_init', 'blankslate_widgets_init' );
function blankslate_widgets_init()
{
    register_sidebar( array (
        'name' => __( 'Bình luận của bạn đọc', 'blankslate' ),
        'id' => 'binhluan',
        'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
        'after_widget' => "</li>",
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
        ) );
}
function blankslate_custom_pings( $comment )
{
    $GLOBALS['comment'] = $comment;
    ?>
    <li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>"><?php echo comment_author_link(); ?></li>
    <?php 
}
add_filter( 'get_comments_number', 'blankslate_comments_number' );
function blankslate_comments_number( $count )
{
    if ( !is_admin() ) {
        global $id;
        $comments_by_type = &separate_comments( get_comments( 'status=approve&post_id=' . $id ) );
        return count( $comments_by_type['comment'] );
    } else {
        return $count;
    }
}

function dusan_kekic_style() {
    wp_enqueue_style( 'mytheme-style', get_stylesheet_uri() ); 
}
add_action( 'wp_enqueue_scripts', 'dusan_kekic_style' );
add_action( 'wp_print_styles', 'my_deregister_styles', 100 );
function my_deregister_styles() {
  wp_deregister_style( 'contact-form-7' );
}
add_action( 'wp_print_styles', 'remove_pagenavi', 100 );
function remove_pagenavi() {
  wp_deregister_style( 'wp-pagenavi' );
}
add_filter( 'wp_default_scripts', 'remove_jquery_migrate' );
function remove_jquery_migrate( &$scripts){
    if(!is_admin()){
        $scripts->remove( 'jquery');
        $scripts->add( 'jquery', false, array( 'jquery-core' ), '1.4.1' );
    }
}

function getPostViews($postID){
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0";
    }
    return $count.'';
}
function setPostViews($postID) {
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
// Remove issues with prefetching adding extra views
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

function excerpt($limit) {

 $excerpt = explode(' ', get_the_excerpt(), $limit);

 if (count($excerpt)>=$limit) {

 array_pop($excerpt);

 $excerpt = implode(" ",$excerpt).'...';

 } else {

 $excerpt = implode(" ",$excerpt);

 }

 $excerpt = preg_replace('`[[^]]*]`','',$excerpt);

 return $excerpt;

}

function content($limit) {

 $content = explode(' ', get_the_content(), $limit);

 if (count($content)>=$limit) {

 array_pop($content);

 $content = implode(" ",$content).'...';

 } else {

 $content = implode(" ",$content);

 }

 $content = preg_replace('/[.+]/','', $content);

 $content = apply_filters('the_content', $content);

 $content = str_replace(']]>', ']]&gt;', $content);

 return $content;

}
// 1. customize ACF path
add_filter('acf/settings/path', 'willgroup_acf_settings_path');
function willgroup_acf_settings_path( $path ) {
	$path = get_stylesheet_directory() . '/inc/acf/';
	return $path;
}

// 2. customize ACF dir
add_filter('acf/settings/dir', 'willgroup_acf_settings_dir');
function willgroup_acf_settings_dir( $dir ) {
	$dir = get_stylesheet_directory_uri() . '/inc/acf/';
	return $dir;
}

// 3. Hide ACF field group menu item
add_filter('acf/settings/show_admin', '__return_false');

// 4. Include ACF
include_once( get_stylesheet_directory() . '/inc/acf/acf.php' );

/**
 * Remove taxonomy slug
 */
require get_template_directory() . '/inc/remove-taxonomy-slug.php';

/**
 * Custom fields
 */
require get_template_directory() . '/inc/post.php';
require get_template_directory() . '/inc/consultant.php';
require get_template_directory() . '/inc/qa.php';

/**
 * Register widget area.
 */
require get_template_directory() . '/inc/widgets.php';
require get_template_directory() . '/inc/widgets/widget-popular-posts.php';
require get_template_directory() . '/inc/widgets/widget-category-posts.php';

/**
 * Ajax functions
 */
require get_template_directory() . '/inc/ajax.php';

/**
 * Utility functions.
 */
require get_template_directory() . '/inc/functions.php';

/**
 * Theme Customizer.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Breadcrumb
 */
require get_template_directory() . '/inc/breadcrumb.php';

/**
 * Load Pagination
 */
require get_template_directory() . '/inc/pagination.php';