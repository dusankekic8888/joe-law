<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width" />
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url')?>/bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url')?>/font-awesome/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>" />
	<script type="text/javascript" src="<?php bloginfo('template_url');?>/js/jquery.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url');?>/jquery-ui/jquery-ui.min.js"></script>

	<script type="text/javascript" src="<?php bloginfo('template_url');?>/js/bootstrap.min.js"></script>

	<script type="text/javascript" src="<?php bloginfo('template_url');?>/js/myjs.js"></script>
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

	<div id="wrapper" class="hfeed">
		<div id="header">
			<div class="container">
				<div class="row">
					<div class="col-md-2">
						<div class="logo-box">
							<a href="/"><img src="<?php bloginfo('template_url');?>/images/logo.png"></a>
						</div>
					</div>
					<div class="col-md-10">
						<?php echo do_shortcode('[ubermenu config_id="main" theme_location="main-menu"]');?>
					</div>
				</div>
			</div>
		</div>

