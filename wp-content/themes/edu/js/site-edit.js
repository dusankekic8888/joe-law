jQuery(function($) {
	
	/*--------------------
	Site navigation scroll
	--------------------*/
	$(window).load(function() {
		adminbarHeight = $('#wpadminbar').length ? $('#wpadminbar').height() : 0;
		navOffset = $('.site-nav').offset().top;
		$(window).on('resize', function() {
			adminbarHeight = $('#wpadminbar').length ? $('#wpadminbar').height() : 0;
			navOffset = $('.site-nav').offset().top;
		});
		$(window).scroll(function () {
			if ( $(window).scrollTop() + adminbarHeight > navOffset ) {
				$('.site-nav').css({ 
					'position' : 'relative',
					'z-index'  : 99,
					'top' 	   : $(window).scrollTop() + adminbarHeight - navOffset 
				});
			} else {
				$('.site-nav').css({ 'top' : 0 });
			}
		});	
	});
	
	/*--------------------
	Site slider
	--------------------*/
	$( '.site-slider' ).slick( {
		dots: true,
		arrows: false,
		infinite: true,
		fade: true,
		speed: 600
	} );
	
	/*--------------------
	scroll top
	--------------------*/
	$('.scroll-top').on('click', function(e) {
		e.preventDefault();
		$('html, body').animate({ 
			scrollTop: '0', 
		}, 400);
	});
	
	/*--------------------
	order
	--------------------*/
	$('[href="#modal-order"]').on('click', function() {
		$('#modal-order').modal('show');
	});
	
	/*--------------------
	Sidebar affix
	--------------------*/
	$(window).load(function() {
		var adminbarHeight = 0;
		if( $('#wpadminbar').length ) {
			adminbarHeight = $('#wpadminbar').height();
		}
		var $scroll = $('.sidebar-affix');
		if ( $scroll.length ) {
			var offsetTop = $scroll.offset().top - adminbarHeight - 15;
			var offsetBottom = $('.site-content').offset().top + $('.site-content').height() - adminbarHeight - $scroll.height() - 40;
			$scroll.css({
				'display': 'block',
				'position': 'relative',
			});
			$(window).scroll(function () {		
				var scrollTop = $(this).scrollTop();
				if ( scrollTop > offsetTop && scrollTop < offsetBottom ) {
					$scroll.css({
						'top': scrollTop - offsetTop,
					});
				} else if ( scrollTop > offsetBottom ) {
					$scroll.css({
						'top': offsetBottom - offsetTop,
					});
				} else {
					$scroll.css({
						'top': '0',
					});
				}
			});
		}
	});
	
	/*--------------------
	Auto open modal after 30s
	--------------------*/
	var modal = setTimeout(function() {
      $('#modal-order').modal('show');
    }, 30000);
	$('#modal-order').on('hidden.bs.modal', function() {
		modal = setTimeout(function() {
		  $('#modal-order').modal('show');
		}, 30000);
	});
	$('#modal-order').on('shown.bs.modal', function() {
		clearTimeout(modal);
	});
});