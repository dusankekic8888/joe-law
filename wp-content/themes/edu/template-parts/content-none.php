<?php
/**
 * Template part for displaying a message that posts cannot be found.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package willgroup
 */

?>

<section class="no-results not-found">
	<?php
	if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

		<p><?php printf( wp_kses( __( 'Sẵn sàng viết bài đầu tiên của bạn? <a href="%1$s">Bắt đầu ở đây.</a>.', 'willgroup' ), array( 'a' => array( 'href' => array() ) ) ), esc_url( admin_url( 'post-new.php' ) ) ); ?></p>

	<?php elseif ( is_search() ) : ?>

		<p><?php esc_html_e( 'Không có kết quả nào thỏa mãn yêu cầu của bạn. Vui lòng thử lại với ô tìm kiếm.', 'willgroup' ); ?></p>
		<?php
			get_search_form();

	else : ?>

		<p><?php esc_html_e( 'Không có kết quả nào thỏa mãn yêu cầu của bạn. Có lẽ tìm kiếm sẽ giúp ích cho bạn.', 'willgroup' ); ?></p>
		<?php
			get_search_form();

	endif; ?>
</section><!-- .no-results -->
